﻿<?xml version='1.0' encoding='UTF-8'?>
<LVClass LVVersion="17008000">
	<Property Name="EndevoGOOP_ClassItemIcon" Type="Str">BlueCircle</Property>
	<Property Name="EndevoGOOP_ClassProvider" Type="Str">Endevo LabVIEW Native</Property>
	<Property Name="EndevoGOOP_ColorFrame" Type="UInt">1179848</Property>
	<Property Name="EndevoGOOP_ColorHeader" Type="UInt">12124142</Property>
	<Property Name="EndevoGOOP_ColorPrivate" Type="UInt">16777215</Property>
	<Property Name="EndevoGOOP_ColorProtected" Type="UInt">16777215</Property>
	<Property Name="EndevoGOOP_ColorPublic" Type="UInt">9341183</Property>
	<Property Name="EndevoGOOP_ColorTextBody" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_ColorTextHeader" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_ConnectorPanePattern" Type="Str">0</Property>
	<Property Name="EndevoGOOP_FrameThickness" Type="UInt">1</Property>
	<Property Name="EndevoGOOP_PlugIns" Type="Str"></Property>
	<Property Name="EndevoGOOP_TemplateUsed" Type="Str">NativeSubTemplate_4x4x4</Property>
	<Property Name="EndevoGOOP_TemplateVersion" Type="Str">1.0.0.1</Property>
	<Property Name="NI.Lib.HelpPath" Type="Str"></Property>
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!,J!!!*Q(C=T:;`&lt;B."%)@H%II5#/3/DMD+!S".B93IX&amp;`FHGJ[+C-B*6O[1++?6\$]"G[AHS[V&amp;?5&amp;X+2!(!&lt;T?&lt;X_=Y[$GS"FTX-Z`X[TM^`NLF=2S?V-Z*EMDL7,UHCE4Y\N1^N@30YM6I&lt;MF#A0O?NC@4PM\T`&gt;[\]?_I"`K*&amp;]@3U0N5@RN[WJG_:BP[G8%^8+W0&amp;8\F\'RN_[L9S.8\@]_8W`W6;9T_@V!&lt;^ET/=0_G1U]X`[R8Z[@N0SGW9](L@HP[YX0K]ZTKW^@P8+XX(XVT^H8)SX^P\_E,LFXP&gt;J/_Z"P^7/_U=;/?FIUH^O)N8[&amp;RW%%U&lt;I[LA)^%!0^%!0^%"X&gt;%&gt;X&gt;%&gt;X&gt;%=X&gt;%-X&gt;%-X&gt;%.8&gt;%68&gt;%68&gt;&amp;W?*MM,8&gt;!&amp;843@--(AQ5""U;"!E!S+",?!*_!*?!)?PEL!%`!%0!&amp;0Q%/+"$Q"4]!4]!1]&gt;*/!*_!*?!+?A)&gt;3G34SB1Z0Q%.Z=8A=(I@(Y8&amp;Y'&amp;)=(A@!'=QJ\"1"1RT4?8"Y("[(BU&gt;R?"Q?B]@B=8CQR?&amp;R?"Q?B]@BI5O?&amp;=]USQM&gt;(MK)Q70Q'$Q'D]&amp;$;4&amp;Y$"[$R_!R?"B/$"[$RY!Q"D3+AS"'*S0"_',Q'$T]%90(Y$&amp;Y$"[$"SOPE/7:7&gt;)M,X2Y&amp;"[&amp;2_&amp;2?"1?3ID#I`!I0!K0QE.:58A5(I6(Y6&amp;Y'%I5(I6(Y6&amp;!F%%:8J2C3E=F32%5(DZZNWB?*=]EGK`SUVRN6#E&lt;5-L'EL*BJ'Q%+1MM:?'E,)C5C:9SA6)G2MI,3XE2+9"3"J:35%KCT,B0C1ER)I&lt;%A/A40;*,&gt;*:&gt;(TFR.JP*&gt;$K6S71CI^&amp;)BM/B$!9$[@@\UOPVJ.PN3K@4W:R7ZVSL6KX0J@4RV&gt;@U?8"T^_MN=5FU&lt;O\1PZ#TDH4SL5JH&lt;[JU`LR+[&lt;2+1HRY5;60\[NU_;&gt;+6Q&gt;CM&lt;B.LX``4#^`X+&lt;4G_^*LODXDJ!=KX0J+:S.=L,[FWE\2X]"J&amp;:7@A!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">385908736</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.1</Property>
	<Property Name="NI.LVClass.ClassNameVisibleInProbe" Type="Bool">true</Property>
	<Property Name="NI.LVClass.CoreWirePen" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!6*0%.M&gt;8.U:8)_$1I]4G&amp;N:4Z1:7Y],UZB&lt;75_$1I]4H6N27RU=TYY0#^/&gt;7V&amp;&lt;(2T0AU+0&amp;5T-DY.#DR/97VF0E:P=G6H=G^V&lt;G1A1W^M&lt;X)],UZB&lt;75_$1I]6G&amp;M0DEX.$!Z-D1],V:B&lt;$Y.#DQP64-S0AU+0&amp;5T-DY.#DR/97VF0E*B9WNH=G^V&lt;G1A1W^M&lt;X)],UZB&lt;75_$1I]6G&amp;M0DAW/41W.D)],V:B&lt;$Y.#DQP64-S0AU+0%.M&gt;8.U:8)_$1I]4G&amp;N:4Z';7RM)&amp;"B&gt;(2F=GY],UZB&lt;75_$1I]4H6N27RU=TYY0#^/&gt;7V&amp;&lt;(2T0AU+0&amp;5Y0AU+0%ZB&lt;75_5G^X)$!],UZB&lt;75_$1I]6G&amp;M0D)V.4QP6G&amp;M0AU+0#^6/$Y.#DR6/$Y.#DR/97VF0F*P&gt;S!R0#^/97VF0AU+0&amp;:B&lt;$YS.45],V:B&lt;$Y.#DQP64A_$1I]64A_$1I]4G&amp;N:4Z3&lt;X=A-DQP4G&amp;N:4Y.#DR797Q_-D5V0#^797Q_$1I],V5Y0AU+0&amp;5Y0AU+0%ZB&lt;75_5G^X)$-],UZB&lt;75_$1I]6G&amp;M0D)V.4QP6G&amp;M0AU+0#^6/$Y.#DR6/$Y.#DR/97VF0F*P&gt;S!U0#^/97VF0AU+0&amp;:B&lt;$YS.45],V:B&lt;$Y.#DQP64A_$1I]64A_$1I]4G&amp;N:4Z3&lt;X=A.4QP4G&amp;N:4Y.#DR797Q_-D5V0#^797Q_$1I],V5Y0AU+0&amp;5Y0AU+0%ZB&lt;75_5G^X)$9],UZB&lt;75_$1I]6G&amp;M0D)V.4QP6G&amp;M0AU+0#^6/$Y.#DR6/$Y.#DR/97VF0F*P&gt;S!X0#^/97VF0AU+0&amp;:B&lt;$YS.45],V:B&lt;$Y.#DQP64A_$1I],U.M&gt;8.U:8)_$1I]34%W0AU+0%ZB&lt;75_6WFE&gt;'A],UZB&lt;75_$1I]6G&amp;M0D%],V:B&lt;$Y.#DQP34%W0AU+0%680AU+0%ZB&lt;75_47^E:4QP4G&amp;N:4Y.#DR$;'^J9W5_1W^Q?4QP1WBP;7.F0AU+0%.I&lt;WFD:4Z0=DQP1WBP;7.F0AU+0%.I&lt;WFD:4Z&amp;?'.M&gt;8.J&gt;G5A4X)],U.I&lt;WFD:4Y.#DR$;'^J9W5_1GFU)%.M:7&amp;S0#^$;'^J9W5_$1I]1WBP;7.F0EZP&gt;#"$&lt;X"Z0#^$;'^J9W5_$1I]1WBP;7.F0EZP&gt;#"0=DQP1WBP;7.F0AU+0%.I&lt;WFD:4Z/&lt;X)A28BD&lt;(6T;8:F)%^S0#^$;'^J9W5_$1I]1WBP;7.F0EZP&gt;#"#;81A1WRF98)],U.I&lt;WFD:4Y.#DR797Q_-$QP6G&amp;M0AU+0#^&amp;6TY.#DR&amp;4$Y.#DR/97VF0F.U?7RF0#^/97VF0AU+0%.I&lt;WFD:4Z4&lt;WRJ:$QP1WBP;7.F0AU+0%.I&lt;WFD:4Z%98.I0#^$;'^J9W5_$1I]1WBP;7.F0E2P&gt;$QP1WBP;7.F0AU+0%.I&lt;WFD:4Z%98.I)%2P&gt;$QP1WBP;7.F0AU+0%.I&lt;WFD:4Z%98.I)%2P&gt;#"%&lt;X1],U.I&lt;WFD:4Y.#DR797Q_-$QP6G&amp;M0AU+0#^&amp;4$Y.#DR&amp;4$Y.#DR/97VF0E:J&lt;'QA5H6M:4QP4G&amp;N:4Y.#DR$;'^J9W5_28:F&lt;C"0:'1],U.I&lt;WFD:4Y.#DR$;'^J9W5_6WFO:'FO:TQP1WBP;7.F0AU+0&amp;:B&lt;$YQ0#^797Q_$1I],U6-0AU+0%6-0AU+0%ZB&lt;75_27ZE)%.B=(-],UZB&lt;75_$1I]1WBP;7.F0E2F:G&amp;V&lt;(1],U.I&lt;WFD:4Y.#DR$;'^J9W5_2GRB&gt;$QP1WBP;7.F0AU+0&amp;:B&lt;$YQ0#^797Q_$1I],U6-0AU+0#^$&lt;(6T&gt;'6S0AU+!!!!!!</Property>
	<Property Name="NI.LVClass.EdgeWirePen" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!5Y0%.M&gt;8.U:8)_$1I]4G&amp;N:4Z1:7Y],UZB&lt;75_$1I]4H6N27RU=TYY0#^/&gt;7V&amp;&lt;(2T0AU+0&amp;5T-DY.#DR/97VF0E:P=G6H=G^V&lt;G1A1W^M&lt;X)],UZB&lt;75_$1I]6G&amp;M0D%T-4%Y.TQP6G&amp;M0AU+0#^6-T)_$1I]64-S0AU+0%ZB&lt;75_1G&amp;D;W&gt;S&lt;X6O:#"$&lt;WRP=DQP4G&amp;N:4Y.#DR797Q_.49W.T1R-4QP6G&amp;M0AU+0#^6-T)_$1I]1WRV=X2F=DY.#DR/97VF0E:J&lt;'QA5'&amp;U&gt;'6S&lt;DQP4G&amp;N:4Y.#DR/&gt;7V&amp;&lt;(2T0DA],UZV&lt;56M&gt;(-_$1I]64A_$1I]4G&amp;N:4Z3&lt;X=A-$QP4G&amp;N:4Y.#DR797Q_-$QP6G&amp;M0AU+0#^6/$Y.#DR6/$Y.#DR/97VF0F*P&gt;S!R0#^/97VF0AU+0&amp;:B&lt;$YQ0#^797Q_$1I],V5Y0AU+0&amp;5Y0AU+0%ZB&lt;75_5G^X)$)],UZB&lt;75_$1I]6G&amp;M0D!],V:B&lt;$Y.#DQP64A_$1I]64A_$1I]4G&amp;N:4Z3&lt;X=A-TQP4G&amp;N:4Y.#DR797Q_-$QP6G&amp;M0AU+0#^6/$Y.#DR6/$Y.#DR/97VF0F*P&gt;S!U0#^/97VF0AU+0&amp;:B&lt;$YQ0#^797Q_$1I],V5Y0AU+0&amp;5Y0AU+0%ZB&lt;75_5G^X)$5],UZB&lt;75_$1I]6G&amp;M0D!],V:B&lt;$Y.#DQP64A_$1I]64A_$1I]4G&amp;N:4Z3&lt;X=A.DQP4G&amp;N:4Y.#DR797Q_-$QP6G&amp;M0AU+0#^6/$Y.#DR6/$Y.#DR/97VF0F*P&gt;S!X0#^/97VF0AU+0&amp;:B&lt;$YQ0#^797Q_$1I],V5Y0AU+0#^$&lt;(6T&gt;'6S0AU+0%ER.DY.#DR/97VF0F&gt;J:(2I0#^/97VF0AU+0&amp;:B&lt;$YT0#^797Q_$1I],UER.DY.#DR&amp;6TY.#DR/97VF0EVP:'5],UZB&lt;75_$1I]1WBP;7.F0E.P=(E],U.I&lt;WFD:4Y.#DR$;'^J9W5_4X)],U.I&lt;WFD:4Y.#DR$;'^J9W5_28BD&lt;(6T;8:F)%^S0#^$;'^J9W5_$1I]1WBP;7.F0E*J&gt;#"$&lt;'6B=DQP1WBP;7.F0AU+0%.I&lt;WFD:4Z/&lt;X1A1W^Q?4QP1WBP;7.F0AU+0%.I&lt;WFD:4Z/&lt;X1A4X)],U.I&lt;WFD:4Y.#DR$;'^J9W5_4G^S)%6Y9WRV=WFW:3"0=DQP1WBP;7.F0AU+0%.I&lt;WFD:4Z/&lt;X1A1GFU)%.M:7&amp;S0#^$;'^J9W5_$1I]6G&amp;M0D!],V:B&lt;$Y.#DQP26=_$1I]25Q_$1I]4G&amp;N:4Z4&gt;(FM:4QP4G&amp;N:4Y.#DR$;'^J9W5_5W^M;71],U.I&lt;WFD:4Y.#DR$;'^J9W5_2'&amp;T;$QP1WBP;7.F0AU+0%.I&lt;WFD:4Z%&lt;X1],U.I&lt;WFD:4Y.#DR$;'^J9W5_2'&amp;T;#"%&lt;X1],U.I&lt;WFD:4Y.#DR$;'^J9W5_2'&amp;T;#"%&lt;X1A2'^U0#^$;'^J9W5_$1I]6G&amp;M0D!],V:B&lt;$Y.#DQP25Q_$1I]25Q_$1I]4G&amp;N:4Z';7RM)&amp;*V&lt;'5],UZB&lt;75_$1I]1WBP;7.F0E6W:7YA4W2E0#^$;'^J9W5_$1I]1WBP;7.F0F&gt;J&lt;G2J&lt;G=],U.I&lt;WFD:4Y.#DR797Q_-$QP6G&amp;M0AU+0#^&amp;4$Y.#DR&amp;4$Y.#DR/97VF0E6O:#"$98"T0#^/97VF0AU+0%.I&lt;WFD:4Z%:7:B&gt;7RU0#^$;'^J9W5_$1I]1WBP;7.F0E:M981],U.I&lt;WFD:4Y.#DR797Q_-$QP6G&amp;M0AU+0#^&amp;4$Y.#DQP1WRV=X2F=DY.#A!!!!!</Property>
	<Property Name="NI.LVClass.FlattenedPrivateDataCTL" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!"I.5F.31QU+!!.-6E.$4%*76Q!!&amp;:A!!!2V!!!!)!!!&amp;8A!!!!:!!!!!225:8.U)(.R&lt;'FU:4-O&lt;(:D&lt;'&amp;T=Q!!!!!!!+!8!)!!!$!!!!A!!!!!!!!%!!-!0!#]!"^!A!)!!!!!"!!"!!&lt;`````!!!!!!!!!!!!!!!!N`Y:@KY!U%O?S@TMO`;U=A!!!!Q!!!!1!!!!!,D8!SI&gt;Q.:,L;CQW]OL&amp;WD5(9T:DQ#S"/G!#:DM_%*_!!!1!!!!!!"=)M&lt;R@6$K4JT'VUSQ9M2+!1!!!0`````5(9T:DQ#S"/G!#:DM_%*_!!!!%0D*J3"(MZG0&lt;[/5I46XS+]!!!!%!!!!!!!!!+1!!5R71U-!!!!#!!*735R#!!!!!&amp;"53$!!!!!&amp;!!%!!1!!!!!#!!*736"*!!!!!!-.=X&amp;M;82F-SZM&gt;GRJ9A^T=7RJ&gt;'5T,GRW9WRB=X-#"Q!!5&amp;2)-!!!!#Y!!1!)!!!!!!:4&lt;X6S9W5(=X&amp;M;82F-Q:4&lt;X6S9W50=X&amp;M;82F-SZM&gt;G.M98.T!!!!!!!"!!!!!!!"!!%!!!!!!A!!!!!!!!!!!Q!!!!)!!1!!!!!!)!!!!"BYH'.A9W"K9,D!!-3-1";4"J$VA5'!!1![)147!!!!%A!!!!BYH'.A9O!!1A9!!&amp;9!%Q!!!!!!3!!!!2BYH'.AQ!4`A1")-4)Q-&amp;U!UCRIYG!;RK9GQ'5O,LOAYMR1.\,#B)(OXA/EG5"S5$610T#&gt;!?)4[/&lt;Q1_E(3')!CR-J$1!!!!Q!!6:*2&amp;-!!!!!!!-!!!'S!!!$K(C=W]$)Q*"J&lt;'('Q-4!Q!RECT-U-#4HJ[4S-A$Z$"#A!W.1!!+AZGGBC2M?/*Q'"(L]]CVA@P-&lt;HGY8&amp;9(G'B5*JF+2&lt;B]6E5Y@&amp;::/&amp;J58@`\``^^]B/&gt;QNU@/=5=&lt;E.JO$K$Y=2=6$B!(3,/![0_"'3"6K/&lt;*&gt;!*FA&lt;1%EA;YA3DW"Q"6=425+$/5M"A?C$J]P-'%%?*1G"/CM,G8?0/&lt;XX!!036Q]#&amp;,&gt;[-'E.]\%51#B8A[1TAEDLNQ[)A"_9QH1!:W]M"]T1(X4RD)A")6A5Y4E%5MD$#,ONG//WC!Q]&amp;""%*F1+A+#&amp;5!IH;!88#%)_YQ00T8PL[XCR6)MS(&amp;C1-1.Y!94+B9DY'2A2(-:'29#V6L!W1T1=6A=1NC+U#$D:("(K\H.F2?!]E=&amp;U;9(I3[;C2X-)(.9'4YQQ!T$WA@6%]$V.UA-6_AW!%I/Q4)HA"F2Q0:([$M*#"&lt;!-L/",).'#(M0#A&lt;&lt;"E$&lt;NL:X]56+:D!_1+?2Y!Y/&lt;@!Q%#PWLH7ONL:JLAQ*\-EV6APJSQH-]E+Q5P/33QONKP6"KEP3#YD5DE$!*37K&gt;]!!!!!!+1!!!$]?*RT9'"AS$3W-'M!UMS-$!TC$!U-S@EJK1R)9!M$&lt;N$]2K$&lt;257EUU7&amp;J^N(2;,42Y5$C&amp;E[762?`0H``X`L!@YJ"`CXH&gt;A".*K"X`6A]R'"QSD[V\[_NQMERYAEZA$%HY%C)$%G)*:'%A=":X]86X4XA&gt;4K!(&amp;S&lt;I'"A6[V=[VVN&lt;..=7&amp;/:EGKM6Z/75ZGEB7#FZS47&amp;RM6QM!&amp;[%PYA!!!*I!!!$M?*RT9'"AS$3W-#M!UMS-$!TC$!U-S@EJK1R)9!E$&lt;N$]2K$&lt;257EUU7&amp;J^N(2;,42Y5$C&amp;E[762?`0H``X`L!8[8!]V("!\$V+^^@7]8U"I'2C1T()$Y,6!%*-9%R.*)YC$A\/`CCOY?E&amp;JN)%YO3#\4KX;ON;ZWNCEOT-EM3487SSH,S5SS1P#3=R+,C_VK'1#K1SQ-!!!!!!!/&amp;Q'!%!!!"D%X,D!O-1!!!!!!!!Q8!)!!!!!%-4=O-!!!!!!/&amp;Q'!%!!!"D%X,D!O-1!!!!!!!!Q8!)!!!!!%-4=O-!!!!!!/&amp;Q'!%!!!"D%X,D!O-1!!!!!!!"1"!!!!^6=VAHEGIIQO=V*/"EEZH1!!!!U!!!!!!!!!!!!!!!!!!!!!!!!!A0````_!=2C0A(NXX`0\/&gt;`T_X\@]`M2X`0```_-!!!"D!!!!9Q%!!'!#]!"A")9!9!C!Y'!1A'"A))$A9'#"I'"]A;"A@Y-A9(`W)'"``#"A@`QA9(``)'"``?"A@`R!9(`]A'"``9"A0`E!9!@[!'!!@!"A!!A!9!!!!(`````!!!#!0`````````````````````Q!!!!$`]!$Q!0]!$Q!0``]!!!!!``]0]0`Q```Q``````!0````$`!0`Q$`]0`````Q$````Q`Q````$`$`````]!````]0]!$Q!0`Q``````!0`````````````````Q!#)!!!!!!!!!!!!!!!!0]!!C!!!!!!!!!!!!!!!!$`!!)A!!!-,-!!!!!!!!!!`Q!!!!!!TG:G,-!!!!!!!0]!!!!!$#:G:G:CT!!!!!$`!!!!!-*G:G:G:G&lt;N!!!!`Q!!!!!G:G:G:G:G9A!!!0]!!!!/:G:G:G:G:O9!!!$`!!!!ZG:G:G:G:GZG!!!!`Q!!!/\O:G:G:G&lt;G:A!!!0]!!!$G:G\G:G:O:G9!!!$`!!!!*G:G:OZGZG:G!!!!`Q!!!#:G:G:G9G:G:A!!!0]!!!!G:G:G:GZG:G)!!!$`!!!!*G:G:G:O:G:C!!!!`Q!!!#:G:G:G&lt;G:G9A!!!0]!!!!G:G:G:GZG:GQ!!!$`!!!!*G:G:G:O:G&lt;!!!!!`Q!!!':G:G:G&lt;G:C!!!!!0]!!!$.ZG:G:GZG)!!!!!$`!!!!!!T?:G:O9M!!!!!!`Q!!!!!!!-XG&lt;GQ!!!!!!0]!!!!!!!!!$.,!!!!!!!$`!!!!!!!!!!!!!!!!!!!!``````````````````````!!!%!0```````````````````````````````````````````S1E*#1E*#1E````!!!!`Q!!!0``!!!!`Q!!!0``````*#1E*#1E*#4`````!0``!0```Q$``````Q$```````````]E*0````````]!``]!!0```Q!!````!0```````````S1E`````````Q$``Q$```````]!``]!````````````*#4`````````!0``!!!!`Q!!!0```Q$```````````]E*0```````````````````````````````````Q!!!.04!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!0``!!!!U^-!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!``]!!!$4UQ!!!!!!!0;D?@A!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!!!!!!$WRXCCH-;&gt;6@9!!!!!!!!!!!!!!0``!!!!!!!!!!!!^K.Y?+*Y?(BYRK.Z+Q!!!!!!!!!!``]!!!!!!!!!!0;D?(BYIHBY?(BY?(C=RY!!!!!!!!$``Q!!!!!!!!!!IXBY?(CC?(BY?(BY?(C=TQ!!!!!!!0``!!!!!!!!!+2Y?(BY?+*Y?(BY?(BYH-@'!!!!!!!!``]!!!!!!!#FH(BY?(BYIHBY?(BY?*T(?-9!!!!!!!$``Q!!!!!!!-@(R]?=?(CC?(BY?(BYRZRYRA!!!!!!!0``!!!!!!!!RZS=H-&lt;(R]:Y?(BY?-?=?(D'!!!!!!!!``]!!!!!!!#DH*S=H*S=RM@(RHD(H(BY?-9!!!!!!!$``Q!!!!!!!+/=H*S=H++=H*T'TJRY?(BYRA!!!!!!!0``!!!!!!!!IZS=H*SCH*S=H*T(H(BY?(CD!!!!!!!!``]!!!!!!!#DH*S=IJS=H*S=H-?CIK*Y?+-!!!!!!!$``Q!!!!!!!+/=H++=H*S=H*S=RZRYIK+CIQ!!!!!!!0``!!!!!!!!IZSCH*S=H*S=H*T(H(BY?*RZ!!!!!!!!``]!!!!!!!#DIJS=H*S=H*S=H-?=?(C=?1!!!!!!!!$``Q!!!!!!!++=H*S=H*S=H*S=RZRYH+-!!!!!!!!!!0``!!!!!!!!_)$(IJS=H*S=H*T(?(CD!!!!!!!!!!!!``]!!!!!!!!!!!$YA-@'H*S=H-&gt;YI`9!!!!!!!!!!!$``Q!!!!!!!!!!!!!!!0B[R];=R]9L!!!!!!!!!!!!!0``!!!!!!!!!!!!!!!!!!!!+XKL+Q!!!!!!!!!!!!!!``]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$```````````````````````````````````````````]!!!%?!!&amp;'5%B1!!!!!A!#2F"131!!!!-.=X&amp;M;82F-SZM&gt;GRJ9A^T=7RJ&gt;'5T,GRW9WRB=X-#"Q!!5&amp;2)-!!!!#Y!!1!)!!!!!!:4&lt;X6S9W5(=X&amp;M;82F-Q:4&lt;X6S9W50=X&amp;M;82F-SZM&gt;G.M98.T!!!!!!!"!!!!!!!"!!%!!!!!!A!!!!!!!!!!!!!!!!%!!!""!!*%2&amp;"*!!!!!!!$$8.R&lt;'FU:4-O&lt;(:M;7)0=X&amp;M;82F-SZM&gt;G.M98.T!A=!!&amp;"53$!!!!!O!!%!#!!!!!!'5W^V=G.F"X.R&lt;'FU:4-'5W^V=G.F$X.R&lt;'FU:4-O&lt;(:D&lt;'&amp;T=Q!!!!!!!1!!!!!!!1!"!!!!!!)!!!!!!!"P!!!!!!!#!!!!11!!!$%!!Q!!!!!#WA!!"D.YH+W5Q5]4124'X[Q,4AGE5U#B!&gt;*+JMA"9AGA##)AAR(3)&amp;AQ[E5&lt;7Z7%C.*C/%&amp;-.C;1=$,B9-+6KY=?^'B-YW5PH$T)I?%`E%1CC7T8.\P&gt;&lt;KF30.D$:L.ZXXNPPO`8!;C]R"K6(+Q91.A"PE1-K)LL"#$433(`;XM$&lt;*&lt;]!F,P*Q&lt;=JL.M6]G2:A/KYXI&lt;$9MV_*[P*LX+).P$UELGRW:6"HDD?I.PAG=:@^`-VSK=LFZI9BMEJ^TDA:^U1VP!A;#VS[?PE_3!C!OKKL6/R:YH.#[`?DKJXWLJ-9!*P7;2:S^C2RT^W7KJ&gt;"#/I`-N!6OW1TK&gt;&gt;E5_7^2GL&gt;'('M+N&gt;4P+;'K&amp;XJ,EW1Z,5W6J=%[P-U=%=G`F\F*5+KU4/E-J[J9&gt;;\AT3Q3-Y&lt;`KHBF1T\-XK*`OR&lt;^6V%RL(Y!!S4SEZE&gt;T3?J^ET)&amp;K\97AQDV%R&lt;&amp;^[A"8:KO0!,6M@=OL&amp;MRK%Y-X4+'GV9-(L('Q-VB`:1=B*JJ&lt;2G&gt;8UKG%IP"B3@"R`/R:$,Y9H(O63S6#-:DK:C&gt;%!P:O[%+?$P"1Y3+1_M2OP_+.%4/NXGJ!R8O1%^R!!OQP&lt;W.HO$4F@;CN)&amp;H#TKP&gt;=#_1ABRVUSZC'PG:41T&gt;"^#$U!=TDS6NI&lt;[F?-QD\AQ8U79@;RUO7-Q$`Q@G%_R[BJCNV,#.Y2B#%&lt;+M$JICVS_M2ZA#M*F..&gt;2MVL-.WL#K"EZH?_B0`C7WB+_.T=X3`E?,E2C5[Y39F/O\:B(ZJ&amp;E`=!]B'E9&gt;FEHV:C4U--37H0@]C*]H/[*%_FO,2^-M"*MQM]G8]\0J2,&gt;`R212/C.84Q&lt;QWFZ#JK1FA(A:&lt;S/WPR\()#,X0JR[[4&lt;9!&lt;&gt;%I=VLWVUTS#[V/_:,*J#&gt;\]M&amp;;ZJ'O8#,&amp;@!RL$NKA(HR'DN*T9OTUD([2D&lt;1I`RPX#?&lt;N'$T.@#2:[*&amp;*4;$KS4:@YO1"N`!YS&lt;KIY!!!!!!!1!!!!H!!!!"!!!!!!!!!!-!!&amp;#2%B1!!!!!!!$!!!!:1!!!(6YH'.A9#A5E'$[RV$XFY&amp;*Y#O1)@W8A6H1D`%X!Q/HH]"B)-UI)!E5FPX,Q#[I$2&lt;70K,,Q1!&amp;KGS-(*)=BQ5ZQ$)=,2I-````6SFCZ0B[Z"J=U2%@/&amp;.FFDS("!"F#"G;!!!!!!!!"!!!!!=!!!,M!!!!"Q!!!#&amp;@&lt;GF@4'&amp;T&gt;%NO&lt;X&gt;O4X&gt;O;7ZH4&amp;:$&lt;'&amp;T=U.M&gt;8.U:8)!!!"S&amp;Q#!!!!!!!%!#!!Q`````Q!"!!!!!!"7!!!!!A!Q1(!!(A!!(QVT=7RJ&gt;'5T,GRW&lt;'FC$X.R&lt;'FU:4-O&lt;(:D&lt;'&amp;T=Q!(=X&amp;M;82F-Q!?1&amp;!!!1!!&amp;&amp;2F=X1A=X&amp;M;82F-SZM&gt;G.M98.T!!!"!!%!!!!!!!!!'ER71WRB=X.1=GFW982F2'&amp;U962B9E^S:'6S!!!!+2=!A!!!!!!#!!5!"Q!!$!"!!!(`````!!!!!1!"!!!!!1!!!!!!!!!!!!!!'UR71WRB=X.1=GFW982F2'&amp;U962J&lt;76T&gt;'&amp;N=!!!!"E8!)!!!!!!!1!&amp;!!=!!!%!!.C4+&lt;5!!!!!!!!!*ER71WRB=X.1=GFW982F2'&amp;U95RB=X2"=("M;76E6'FN:8.U97VQ!!!!'2=!A!!!!!!"!!5!"Q!!!1!!W*-JN1!!!!!!!!!;4&amp;:$&lt;'&amp;T=V"S;8:B&gt;'6%982B6(FQ:52F=W-!!!"S&amp;Q#!!!!!!!%!#!!Q`````Q!"!!!!!!"7!!!!!A!Q1(!!(A!!(QVT=7RJ&gt;'5T,GRW&lt;'FC$X.R&lt;'FU:4-O&lt;(:D&lt;'&amp;T=Q!(=X&amp;M;82F-Q!?1&amp;!!!1!!&amp;&amp;2F=X1A=X&amp;M;82F-SZM&gt;G.M98.T!!!"!!%!!!!!!!!!(ER71WRB=X.1=GFW982F2'&amp;U952G&lt;(2%982B5WF[:1!!!"E8!)!!!!!!!1!&amp;!!-!!!%!!!!!!!1!!!!!!!!!'ER71WRB=X.1=GFW982F2'&amp;U952G&lt;(2%982B!!!!CB=!A!!!!!!#!$"!=!!?!!!@$8.R&lt;'FU:4-O&lt;(:M;7)0=X&amp;M;82F-SZM&gt;G.M98.T!!&gt;T=7RJ&gt;'5T!"Z!5!!"!!!56'6T&gt;#"T=7RJ&gt;'5T,GRW9WRB=X-!!!%!!1!!!!%@$8.R&lt;'FU:4-O&lt;(:M;7)0=X&amp;M;82F-SZM&gt;G.M98.T!!!!!!!!!!!!!!!!!!!!!!1!!Q!+!!!!"!!!!&amp;1!!!!I!!!!!A!!"!!!!!!J!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!0M!!!'^?*S.DU&amp;+QV!1BL`EV3;ND9U;F_J&lt;C&lt;K11C]1%&amp;Q8=?(3W,R)Y%&amp;K]VJ=?A@0YFU]BN\!;2IJ[K&lt;]--R]-]T]!RQR3G?=Q'F50^P3G@'68&gt;LS=&lt;CJJD;L;Y)7=-`8R^P&amp;/_!H&gt;[:W_M^E`!N/H?5SH?$"]&lt;6&gt;V-\-&gt;68I:F,0ZO5S=U&lt;HG=P9%&gt;%H"_]48QL6FITJU%M$6''@#..86&gt;W[.4"613T&lt;&amp;6U#1L8)#]\:[B]'D;O_=C_ZX*H)S2[\H'XJV5&gt;#&gt;R6E/O2"&lt;(;YE47;09&lt;38CE7^C/`V8_S[?QX7D`N=3"%&lt;%KGZ-S!C%-3)9FE%8Q$L&lt;N32!!!!!"R!!%!!A!$!!5!!!"9!!U%!!!!!!U!U!#Z!!!!8Q!."!!!!!!.!.!!O1!!!'9!$11!!!!!$1$1!,E!!!"NA!#%!)!!!!U!U!#Z!!!!&lt;Y!!B!#!!!!.!.!!O1:597BP&lt;7%'6'&amp;I&lt;WVB"F2B;'^N91%S!4!!!!"35V*$$1I!!UR71U.-1F:8!!!6G!!!"(5!!!!A!!!6?!!!!!!!!!!!!!!!)!!!!$1!!!2E!!!!(%R*1EY!!!!!!!!"9%R75V)!!!!!!!!"&gt;&amp;*55U=!!!!!!!!"C%.$5V1!!!!!!!!"H%R*&gt;GE!!!!!!!!"M%.04F!!!!!!!!!"R&amp;2./$!!!!!"!!!"W%2'2&amp;-!!!!!!!!#!%R*:(-!!!!!!!!#&amp;&amp;:*1U1!!!!#!!!#+(:F=H-!!!!%!!!#:&amp;.$5V)!!!!!!!!#S%&gt;$5&amp;)!!!!!!!!#X%F$4UY!!!!!!!!#]'FD&lt;$1!!!!!!!!$"'FD&lt;$A!!!!!!!!$'%R*:H!!!!!!!!!$,%:13')!!!!!!!!$1%:15U5!!!!!!!!$6&amp;:12&amp;!!!!!!!!!$;%R*9G1!!!!!!!!$@%*%3')!!!!!!!!$E%*%5U5!!!!!!!!$J&amp;:*6&amp;-!!!!!!!!$O%253&amp;!!!!!!!!!$T%V6351!!!!!!!!$Y%B*5V1!!!!!!!!$^&amp;:$6&amp;!!!!!!!!!%#%:515)!!!!!!!!%(!!!!!$`````!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!)!!!!!!!!!!!`````Q!!!!!!!!$%!!!!!!!!!!$`````!!!!!!!!!.A!!!!!!!!!!0````]!!!!!!!!!Y!!!!!!!!!!!`````Q!!!!!!!!')!!!!!!!!!!$`````!!!!!!!!!:!!!!!!!!!!!P````]!!!!!!!!"N!!!!!!!!!!!`````Q!!!!!!!!(-!!!!!!!!!!$`````!!!!!!!!!BA!!!!!!!!!!0````]!!!!!!!!#+!!!!!!!!!!"`````Q!!!!!!!!0A!!!!!!!!!!,`````!!!!!!!!")A!!!!!!!!!"0````]!!!!!!!!&amp;+!!!!!!!!!!(`````Q!!!!!!!!5]!!!!!!!!!!D`````!!!!!!!!"5Q!!!!!!!!!#@````]!!!!!!!!&amp;9!!!!!!!!!!+`````Q!!!!!!!!6Q!!!!!!!!!!$`````!!!!!!!!"91!!!!!!!!!!0````]!!!!!!!!&amp;H!!!!!!!!!!!`````Q!!!!!!!!7Q!!!!!!!!!!$`````!!!!!!!!"D1!!!!!!!!!!0````]!!!!!!!!)/!!!!!!!!!!!`````Q!!!!!!!!Q]!!!!!!!!!!$`````!!!!!!!!$7!!!!!!!!!!!0````]!!!!!!!!11!!!!!!!!!!!`````Q!!!!!!!"")!!!!!!!!!!$`````!!!!!!!!%&amp;!!!!!!!!!!!0````]!!!!!!!!19!!!!!!!!!!!`````Q!!!!!!!"$-!!!!!!!!!!$`````!!!!!!!!%.1!!!!!!!!!!0````]!!!!!!!!4R!!!!!!!!!!!`````Q!!!!!!!"0-!!!!!!!!!!$`````!!!!!!!!%^1!!!!!!!!!!0````]!!!!!!!!5!!!!!!!!!!#!`````Q!!!!!!!"5!!!!!!""5:8.U)(.R&lt;'FU:4-O9X2M!!!!!!</Property>
	<Property Name="NI.LVClass.Geneology" Type="Xml"><String>

<Name></Name>

<Val>!!!!!225:8.U)(.R&lt;'FU:4-O&lt;(:D&lt;'&amp;T=Q"16%AQ!!!!!!!!!!!!!!!!!!-!!1!!!!!!!!%!!!!#!!J!)12/&lt;WZF!!!)!&amp;!!!1!!!!%!!1!!!!!!!!!!!2"5:8.U1W&amp;T:3ZM&gt;G.M98.T!&amp;"53$!!!!!!!!!!!!!))9!#!!!!!!!!!!!!!!%!!!!!!!!!!!!!!A!+1#%%4G^O:1!!#!"1!!%!!!!"!!%!!!!"`````A!!!!!"%&amp;2F=X2$98.F,GRW9WRB=X-!5&amp;2)-!!!!!!!!!!!!"=!A!!!!!!!!!!!!!!!!1!!!!!!!1!!!!!#!$"!=!!?!!!@$8.R&lt;'FU:4-O&lt;(:M;7)0=X&amp;M;82F-SZM&gt;G.M98.T!!&gt;T=7RJ&gt;'5T!&amp;A!]&gt;C4+&lt;5!!!!#&amp;&amp;2F=X1A=X&amp;M;82F-SZM&gt;G.M98.T%&amp;2F=X1A=X&amp;M;82F-SZD&gt;'Q!+E"1!!%!!"V$&lt;(6T&gt;'6S)'^G)'.M98.T)("S;8:B&gt;'5A:'&amp;U91!"!!%!!!!"`````Q!!!!%@$8.R&lt;'FU:4-O&lt;(:M;7)0=X&amp;M;82F-SZM&gt;G.M98.T!!!!!!!!!!!!!!!!!!!!!2"5:8.U1W&amp;T:3ZM&gt;G.M98.T!&amp;"53$!!!!!!!!!!!!!8!)!!!!!!!!!!!!!!!1!!!"B5:7VQ&lt;'&amp;U:62F=X2$98.F,GRW9WRB=X-</Val>

</String>

</Property>
	<Property Name="NI.LVClass.IsTransferClass" Type="Bool">false</Property>
	<Property Name="NI.LVClass.LowestCompatibleVersion" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LVClass.ParentClassLinkInfo" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!"H!!!!!2"5:8.U1W&amp;T:3ZM&gt;G.M98.T!&amp;"53$!!!!"*!!!!"A=]&gt;GFM;7)_"G&amp;E:'^O=QV@3EN*)&amp;2P&lt;WRL;82T#6:*)&amp;2F=X2F=AR5:8.U1W&amp;T:3ZM&lt;')16'6T&gt;%.B=W5O&lt;(:D&lt;'&amp;T=Q!!!!!</Property>
	<Property Name="NI.SortType" Type="Int">3</Property>
	<Item Name="Test sqlite3.ctl" Type="Class Private Data" URL="Test sqlite3.ctl">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
	</Item>
	<Item Name="private" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
	</Item>
	<Item Name="protected" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">3</Property>
	</Item>
	<Item Name="setUp.vi" Type="VI" URL="../setUp.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%G!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#Z!=!!?!!!7&amp;&amp;2F=X1A=X&amp;M;82F-SZM&gt;G.M98.T!!!.=G6G:8*F&lt;G.F)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!#Z!=!!?!!!7&amp;&amp;2F=X1A=X&amp;M;82F-SZM&gt;G.M98.T!!!-=G6G:8*F&lt;G.F)'FO!!"B!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!1!"Q-!!(A!!!U)!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!!!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!)!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1350574608</Property>
	</Item>
	<Item Name="tearDown.vi" Type="VI" URL="../tearDown.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%G!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#Z!=!!?!!!7&amp;&amp;2F=X1A=X&amp;M;82F-SZM&gt;G.M98.T!!!.=G6G:8*F&lt;G.F)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!#Z!=!!?!!!7&amp;&amp;2F=X1A=X&amp;M;82F-SZM&gt;G.M98.T!!!-=G6G:8*F&lt;G.F)'FO!!"B!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!1!"Q)!!(A!!!E!!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!!!!!!%!!!$1!!!!Q!!!!!!!!!!!!!!1!)!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1082139136</Property>
	</Item>
	<Item Name="Test_sqlite3Open_worksNoError.vi" Type="VI" URL="../Test_sqlite3Open_worksNoError.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%E!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#Z!=!!?!!!7&amp;&amp;2F=X1A=X&amp;M;82F-SZM&gt;G.M98.T!!!-6'6T&gt;%.B=W5A&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!#R!=!!?!!!7&amp;&amp;2F=X1A=X&amp;M;82F-SZM&gt;G.M98.T!!!,6'6T&gt;%.B=W5A;7Y!91$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!%!!=$!!"Y!!!.#!!!!!!!!!!!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!")!!!U!!!!-!!!!!!!!!!!!!!%!#!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1350574608</Property>
	</Item>
	<Item Name="Test_sqlite3Open_ErrorIfNotaPath.vi" Type="VI" URL="../Test_sqlite3Open_ErrorIfNotaPath.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%E!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#Z!=!!?!!!7&amp;&amp;2F=X1A=X&amp;M;82F-SZM&gt;G.M98.T!!!-6'6T&gt;%.B=W5A&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!#R!=!!?!!!7&amp;&amp;2F=X1A=X&amp;M;82F-SZM&gt;G.M98.T!!!,6'6T&gt;%.B=W5A;7Y!91$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!%!!=$!!"Y!!!.#!!!!!!!!!!!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!")!!!U!!!!-!!!!!!!!!!!!!!%!#!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">32</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1074278928</Property>
	</Item>
	<Item Name="Test_sqlite3ListColumns_ListsColumns.vi" Type="VI" URL="../Test_sqlite3ListColumns_ListsColumns.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%E!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#Z!=!!?!!!7&amp;&amp;2F=X1A=X&amp;M;82F-SZM&gt;G.M98.T!!!-6'6T&gt;%.B=W5A&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!#R!=!!?!!!7&amp;&amp;2F=X1A=X&amp;M;82F-SZM&gt;G.M98.T!!!,6'6T&gt;%.B=W5A;7Y!91$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!%!!=$!!"Y!!!.#!!!!!!!!!!!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!")!!!U!!!!-!!!!!!!!!!!!!!%!#!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">32</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1074278928</Property>
	</Item>
	<Item Name="Test_sqlite3ListTables_ListsTables.vi" Type="VI" URL="../Test_sqlite3ListTables_ListsTables.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%E!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#Z!=!!?!!!7&amp;&amp;2F=X1A=X&amp;M;82F-SZM&gt;G.M98.T!!!-6'6T&gt;%.B=W5A&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!#R!=!!?!!!7&amp;&amp;2F=X1A=X&amp;M;82F-SZM&gt;G.M98.T!!!,6'6T&gt;%.B=W5A;7Y!91$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!%!!=$!!"Y!!!.#!!!!!!!!!!!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!")!!!U!!!!-!!!!!!!!!!!!!!%!#!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">16777219</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1074541072</Property>
	</Item>
	<Item Name="Test_sqlite3TableMetadata_ListsTableMetaData.vi" Type="VI" URL="../Test_sqlite3TableMetadata_ListsTableMetaData.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%E!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#Z!=!!?!!!7&amp;&amp;2F=X1A=X&amp;M;82F-SZM&gt;G.M98.T!!!-6'6T&gt;%.B=W5A&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!#R!=!!?!!!7&amp;&amp;2F=X1A=X&amp;M;82F-SZM&gt;G.M98.T!!!,6'6T&gt;%.B=W5A;7Y!91$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!%!!=$!!"Y!!!.#!!!!!!!!!!!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!")!!!U!!!!-!!!!!!!!!!!!!!%!#!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">16777219</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1074541072</Property>
	</Item>
	<Item Name="Test_sqlite3QueryNotAQuery_ReturnsError.vi" Type="VI" URL="../Test_sqlite3QueryNotAQuery_ReturnsError.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%E!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#Z!=!!?!!!7&amp;&amp;2F=X1A=X&amp;M;82F-SZM&gt;G.M98.T!!!-6'6T&gt;%.B=W5A&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!#R!=!!?!!!7&amp;&amp;2F=X1A=X&amp;M;82F-SZM&gt;G.M98.T!!!,6'6T&gt;%.B=W5A;7Y!91$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!%!!=$!!"Y!!!.#!!!!!!!!!!!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!")!!!U!!!!-!!!!!!!!!!!!!!%!#!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">16777219</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1074541072</Property>
	</Item>
	<Item Name="Test_sqlite3QueriesWithNoDatabase_ReturnsError.vi" Type="VI" URL="../Test_sqlite3QueriesWithNoDatabase_ReturnsError.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%E!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#Z!=!!?!!!7&amp;&amp;2F=X1A=X&amp;M;82F-SZM&gt;G.M98.T!!!-6'6T&gt;%.B=W5A&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!#R!=!!?!!!7&amp;&amp;2F=X1A=X&amp;M;82F-SZM&gt;G.M98.T!!!,6'6T&gt;%.B=W5A;7Y!91$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!%!!=$!!"Y!!!.#!!!!!!!!!!!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!")!!!U!!!!-!!!!!!!!!!!!!!%!#!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1342972432</Property>
	</Item>
	<Item Name="Test_sqlite3QueryWorks_NoError.vi" Type="VI" URL="../Test_sqlite3QueryWorks_NoError.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%E!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#Z!=!!?!!!7&amp;&amp;2F=X1A=X&amp;M;82F-SZM&gt;G.M98.T!!!-6'6T&gt;%.B=W5A&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!#R!=!!?!!!7&amp;&amp;2F=X1A=X&amp;M;82F-SZM&gt;G.M98.T!!!,6'6T&gt;%.B=W5A;7Y!91$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!%!!=$!!"Y!!!.#!!!!!!!!!!!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!")!!!U!!!!-!!!!!!!!!!!!!!%!#!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">16777219</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1074541072</Property>
	</Item>
	<Item Name="Test_sqlite3InvalidQuery_ReturnsError.vi" Type="VI" URL="../Test_sqlite3InvalidQuery_ReturnsError.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%E!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#Z!=!!?!!!7&amp;&amp;2F=X1A=X&amp;M;82F-SZM&gt;G.M98.T!!!-6'6T&gt;%.B=W5A&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!#R!=!!?!!!7&amp;&amp;2F=X1A=X&amp;M;82F-SZM&gt;G.M98.T!!!,6'6T&gt;%.B=W5A;7Y!91$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!%!!=$!!"Y!!!.#!!!!!!!!!!!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!")!!!U!!!!-!!!!!!!!!!!!!!%!#!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">16777219</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1074541072</Property>
	</Item>
	<Item Name="Test_sqlite3QueryVIM2DStringWorks_NoError.vi" Type="VI" URL="../Test_sqlite3QueryVIM2DStringWorks_NoError.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%E!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#Z!=!!?!!!7&amp;&amp;2F=X1A=X&amp;M;82F-SZM&gt;G.M98.T!!!-6'6T&gt;%.B=W5A&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!#R!=!!?!!!7&amp;&amp;2F=X1A=X&amp;M;82F-SZM&gt;G.M98.T!!!,6'6T&gt;%.B=W5A;7Y!91$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!%!!=$!!"Y!!!.#!!!!!!!!!!!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!")!!!U!!!!-!!!!!!!!!!!!!!%!#!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1342710288</Property>
	</Item>
	<Item Name="Test_sqlite3QueryVIM1DStringWorks_NoError.vi" Type="VI" URL="../Test_sqlite3QueryVIM1DStringWorks_NoError.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%E!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#Z!=!!?!!!7&amp;&amp;2F=X1A=X&amp;M;82F-SZM&gt;G.M98.T!!!-6'6T&gt;%.B=W5A&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!#R!=!!?!!!7&amp;&amp;2F=X1A=X&amp;M;82F-SZM&gt;G.M98.T!!!,6'6T&gt;%.B=W5A;7Y!91$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!%!!=$!!"Y!!!.#!!!!!!!!!!!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!")!!!U!!!!-!!!!!!!!!!!!!!%!#!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">32</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1074278928</Property>
	</Item>
	<Item Name="Test_sqlite3QueryVIMScalarStringWorks_NoError.vi" Type="VI" URL="../Test_sqlite3QueryVIMScalarStringWorks_NoError.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%E!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#Z!=!!?!!!7&amp;&amp;2F=X1A=X&amp;M;82F-SZM&gt;G.M98.T!!!-6'6T&gt;%.B=W5A&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!#R!=!!?!!!7&amp;&amp;2F=X1A=X&amp;M;82F-SZM&gt;G.M98.T!!!,6'6T&gt;%.B=W5A;7Y!91$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!%!!=$!!"Y!!!.#!!!!!!!!!!!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!")!!!U!!!!-!!!!!!!!!!!!!!%!#!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">16777225</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1074541072</Property>
	</Item>
	<Item Name="Test_sqlite3QueryVIM2DIntegerWorks_NoError.vi" Type="VI" URL="../Test_sqlite3QueryVIM2DIntegerWorks_NoError.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%E!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#Z!=!!?!!!7&amp;&amp;2F=X1A=X&amp;M;82F-SZM&gt;G.M98.T!!!-6'6T&gt;%.B=W5A&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!#R!=!!?!!!7&amp;&amp;2F=X1A=X&amp;M;82F-SZM&gt;G.M98.T!!!,6'6T&gt;%.B=W5A;7Y!91$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!%!!=$!!"Y!!!.#!!!!!!!!!!!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!")!!!U!!!!-!!!!!!!!!!!!!!%!#!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">32</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1074278928</Property>
	</Item>
	<Item Name="Test_sqlite3QueryVIM1DIntegerWorks_NoError.vi" Type="VI" URL="../Test_sqlite3QueryVIM1DIntegerWorks_NoError.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%E!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#Z!=!!?!!!7&amp;&amp;2F=X1A=X&amp;M;82F-SZM&gt;G.M98.T!!!-6'6T&gt;%.B=W5A&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!#R!=!!?!!!7&amp;&amp;2F=X1A=X&amp;M;82F-SZM&gt;G.M98.T!!!,6'6T&gt;%.B=W5A;7Y!91$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!%!!=$!!"Y!!!.#!!!!!!!!!!!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!")!!!U!!!!-!!!!!!!!!!!!!!%!#!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">16777227</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1074541072</Property>
	</Item>
	<Item Name="Test_sqlite3QueryVIMScalarIntegerWorks_NoError.vi" Type="VI" URL="../Test_sqlite3QueryVIMScalarIntegerWorks_NoError.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%E!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#Z!=!!?!!!7&amp;&amp;2F=X1A=X&amp;M;82F-SZM&gt;G.M98.T!!!-6'6T&gt;%.B=W5A&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!#R!=!!?!!!7&amp;&amp;2F=X1A=X&amp;M;82F-SZM&gt;G.M98.T!!!,6'6T&gt;%.B=W5A;7Y!91$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!%!!=$!!"Y!!!.#!!!!!!!!!!!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!")!!!U!!!!-!!!!!!!!!!!!!!%!#!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">16777227</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1074541072</Property>
	</Item>
	<Item Name="Test_sqlite3QueryVIMScalarDoubleWorks_NoError.vi" Type="VI" URL="../Test_sqlite3QueryVIMScalarDoubleWorks_NoError.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%E!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#Z!=!!?!!!7&amp;&amp;2F=X1A=X&amp;M;82F-SZM&gt;G.M98.T!!!-6'6T&gt;%.B=W5A&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!#R!=!!?!!!7&amp;&amp;2F=X1A=X&amp;M;82F-SZM&gt;G.M98.T!!!,6'6T&gt;%.B=W5A;7Y!91$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!%!!=$!!"Y!!!.#!!!!!!!!!!!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!")!!!U!!!!-!!!!!!!!!!!!!!%!#!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">16777227</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1074541072</Property>
	</Item>
	<Item Name="Test_sqlite3QueryVIM1DDoubleWorks_NoError.vi" Type="VI" URL="../Test_sqlite3QueryVIM1DDoubleWorks_NoError.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%E!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#Z!=!!?!!!7&amp;&amp;2F=X1A=X&amp;M;82F-SZM&gt;G.M98.T!!!-6'6T&gt;%.B=W5A&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!#R!=!!?!!!7&amp;&amp;2F=X1A=X&amp;M;82F-SZM&gt;G.M98.T!!!,6'6T&gt;%.B=W5A;7Y!91$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!%!!=$!!"Y!!!.#!!!!!!!!!!!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!")!!!U!!!!-!!!!!!!!!!!!!!%!#!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">16777227</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1074541072</Property>
	</Item>
	<Item Name="Test_sqlite3QueryVIM2DDoubleWorks_NoError.vi" Type="VI" URL="../Test_sqlite3QueryVIM2DDoubleWorks_NoError.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%E!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#Z!=!!?!!!7&amp;&amp;2F=X1A=X&amp;M;82F-SZM&gt;G.M98.T!!!-6'6T&gt;%.B=W5A&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!#R!=!!?!!!7&amp;&amp;2F=X1A=X&amp;M;82F-SZM&gt;G.M98.T!!!,6'6T&gt;%.B=W5A;7Y!91$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!%!!=$!!"Y!!!.#!!!!!!!!!!!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!")!!!U!!!!-!!!!!!!!!!!!!!%!#!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">16777227</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1074541072</Property>
	</Item>
	<Item Name="Test_sqlite3QueryVIMScalar64Works_NoError.vi" Type="VI" URL="../Test_sqlite3QueryVIMScalar64Works_NoError.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%E!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#Z!=!!?!!!7&amp;&amp;2F=X1A=X&amp;M;82F-SZM&gt;G.M98.T!!!-6'6T&gt;%.B=W5A&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!#R!=!!?!!!7&amp;&amp;2F=X1A=X&amp;M;82F-SZM&gt;G.M98.T!!!,6'6T&gt;%.B=W5A;7Y!91$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!%!!=$!!"Y!!!.#!!!!!!!!!!!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!")!!!U!!!!-!!!!!!!!!!!!!!%!#!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">16777227</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1074541072</Property>
	</Item>
	<Item Name="Test_sqlite3QueryVIM1D64Works_NoError.vi" Type="VI" URL="../Test_sqlite3QueryVIM1D64Works_NoError.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%E!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#Z!=!!?!!!7&amp;&amp;2F=X1A=X&amp;M;82F-SZM&gt;G.M98.T!!!-6'6T&gt;%.B=W5A&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!#R!=!!?!!!7&amp;&amp;2F=X1A=X&amp;M;82F-SZM&gt;G.M98.T!!!,6'6T&gt;%.B=W5A;7Y!91$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!%!!=$!!"Y!!!.#!!!!!!!!!!!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!")!!!U!!!!-!!!!!!!!!!!!!!%!#!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">16777227</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1074541072</Property>
	</Item>
	<Item Name="Test_sqlite3QueryVIM2D64Works_NoError.vi" Type="VI" URL="../Test_sqlite3QueryVIM2D64Works_NoError.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%E!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#Z!=!!?!!!7&amp;&amp;2F=X1A=X&amp;M;82F-SZM&gt;G.M98.T!!!-6'6T&gt;%.B=W5A&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!#R!=!!?!!!7&amp;&amp;2F=X1A=X&amp;M;82F-SZM&gt;G.M98.T!!!,6'6T&gt;%.B=W5A;7Y!91$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!%!!=$!!"Y!!!.#!!!!!!!!!!!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!")!!!U!!!!-!!!!!!!!!!!!!!%!#!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">16777227</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1074541072</Property>
	</Item>
	<Item Name="Test_sqlite3CreateDatabaseAndATableWorks_NoError.vi" Type="VI" URL="../Test_sqlite3CreateDatabaseAndATableWorks_NoError.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%E!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#Z!=!!?!!!7&amp;&amp;2F=X1A=X&amp;M;82F-SZM&gt;G.M98.T!!!-6'6T&gt;%.B=W5A&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!#R!=!!?!!!7&amp;&amp;2F=X1A=X&amp;M;82F-SZM&gt;G.M98.T!!!,6'6T&gt;%.B=W5A;7Y!91$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!%!!=$!!"Y!!!.#!!!!!!!!!!!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!")!!!U!!!!-!!!!!!!!!!!!!!%!#!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">32</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1074278928</Property>
	</Item>
	<Item Name="Test_sqlite3QueryVIMVariantBinaryDataWorks_NoError.vi" Type="VI" URL="../Test_sqlite3QueryVIMVariantBinaryDataWorks_NoError.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%E!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#Z!=!!?!!!7&amp;&amp;2F=X1A=X&amp;M;82F-SZM&gt;G.M98.T!!!-6'6T&gt;%.B=W5A&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!#R!=!!?!!!7&amp;&amp;2F=X1A=X&amp;M;82F-SZM&gt;G.M98.T!!!,6'6T&gt;%.B=W5A;7Y!91$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!%!!=$!!"Y!!!.#!!!!!!!!!!!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!")!!!U!!!!-!!!!!!!!!!!!!!%!#!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">32</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1074278928</Property>
	</Item>
</LVClass>
