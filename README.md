# README #

**composed-database** is a malleable interface for multiple database engines to handle authentication, connection management and query support.  Source is LabVIEW 2017.
Included is a reference implementation for sqlite.

### How do I get set up?
- Install this package using [GPM](https://gpackage.io) and start coding
- All public API methods are clearly marked in the souce library (IDatabase.lvlib)

#### Examples
- No examples provided at this time

#### Dependencies
- This package depends only on base *vi.lib*
- This distribution includes precompiled public domain sqlite binaries for Windows 32/64 bit version 3.27.2  Check out their awesome database at www.sqlite.org

### Contribution guidelines
- Please contact the author if you want to contribute.
- All contributions must adhere to SOLID design principles.
- All code must be unit tested to the extent reasonably possible.

### Who do I talk to?
- Steve Ball | Composed Systems, LLC
- Steve.Ball@composed.io

### License
- See license file