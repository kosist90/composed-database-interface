﻿<?xml version='1.0' encoding='UTF-8'?>
<LVClass LVVersion="17008000">
	<Property Name="EndevoGOOP_ColorFrame" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_ColorHeader" Type="UInt">16776704</Property>
	<Property Name="EndevoGOOP_ColorPrivate" Type="UInt">9868950</Property>
	<Property Name="EndevoGOOP_ColorProtected" Type="UInt">13816530</Property>
	<Property Name="EndevoGOOP_ColorPublic" Type="UInt">16448250</Property>
	<Property Name="EndevoGOOP_ColorTextBody" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_ColorTextHeader" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_FrameThickness" Type="UInt">1</Property>
	<Property Name="NI.Lib.ContainingLib" Type="Str">sqlite3.lvlib</Property>
	<Property Name="NI.Lib.ContainingLibPath" Type="Str">../sqlite3.lvlib</Property>
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!,:!!!*Q(C=T:1\DB."%):L6YR%B/14A+T.E"C*3B'2IQW2(-Q&amp;[AL/Q*O!)`,+C3T0#2RN2&amp;*8=,!8M%3)B"C_[7E`RK]F!'G\8@&lt;-`V&gt;8@^0&gt;(J(5HIM]E_;R6FRMH:`35L1X[&lt;&lt;:/%7H&amp;MFN.B&gt;&amp;UR^`\$&gt;\@G&gt;N[R_/&lt;`)M??9]PNE@@Z\`M398W[&amp;@&amp;*@]ICKLKDDHY\:)P9S&gt;H^W$D+W`=\O-1\`M_@7R8_UKV(6&gt;HP"T2FW@^=GI[IN_NJ_?8`8]KFIM&amp;PXV,]ONTW-O5OPP8^HZ?_\B`K?-9L'TD]Z0W8/0@&gt;K??^,PN&lt;]ZXN0(`Q4`NYF=*6)C##?-U02U4;!(?K!(?K!(OK-\OK-\OK-\OK%&lt;OK%&lt;OK%&lt;OK)LOK)LOK)L?OLIAC\IAM[K"*-(%Q6&amp;AQ*"-CA3@!5]!5`!%`"Q+Q&amp;0Q"0Q"$Q"$SE3]!1]!5`!%`!Q4!+?A#@A#8A#(EIFEEA&gt;(:[!B`,C]$A]$I`$Y`!QJ4A]$I!TG609+1+'/+:TY@!Y0!Y0F_,Q/$Q/D]0D]'#,Q_0Q/$Q/D]0$E,1KHGD;DAY0:=4A-8A-(I0(Y+'U'$Q'D]&amp;D]"A]4#='D]&amp;D1"A4'M6"%'/1E7$='$Q'$T^C]"A]"I`"9`"AJ2WSN$)N4&gt;P2Y6&amp;Y&amp;"[&amp;2_&amp;2?#AB#I`#I`!I0!I0:58B58A5(I6(Y7%K58A5(I6(!6%G:8J2CCE$F32&amp;5(DYJ./C;:=]E7DK_;`:(64*"V$SQ:*]9#1@"-E&lt;,(HD*'_)Z)77P)#3&amp;U&lt;S!UN_%-G!EC?78&amp;"SIKTZ8B&amp;,9E\-C!ER*E&lt;%E"CU1`^RYHK^FN6K*=PF5O&lt;TO=RG-ZF-*D)?DW5U'MFQ/*4"9,"^7\WC&gt;_VK]V[;=PXF\:O(W?X.X?@&lt;G`O\$T&gt;X8V^`?WDVD8][LIE8R(PC)`(\20SCTI_@5`H/\]P\K8R#?X?VK@UUXIVS,=X?;ZYV_A0M*]6&gt;!!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">385908736</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.1</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="NI.LVClass.ClassNameVisibleInProbe" Type="Bool">true</Property>
	<Property Name="NI.LVClass.DataValRefToSelfLimitedLibFlag" Type="Bool">true</Property>
	<Property Name="NI.LVClass.FlattenedPrivateDataCTL" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!"@!5F.31QU+!!.-6E.$4%*76Q!!%XA!!!2)!!!!)!!!%VA!!!!C!!!!!AVT=7RJ&gt;'5T,GRW&lt;'FC$X.R&lt;'FU:4-O&lt;(:D&lt;'&amp;T=Q!!!!!!I"=!A!!!-!!!#!!%!!!!!!1!!Q!]!,Q!(U#!!A!!!!!%!!%!"P````]!!!!!!!!!!!!!!!!.W-D.F!:T39JGRMM;[9-Y!!!!$!!!!"!!!!!$U]B25I&gt;J-%G@NED?OAH-$N1&gt;D.G0!,)%[9!*G/TY1HY!!"!!!!!!!+L+W`1TCOR"L'"MR7HT3+U"!!!!`````^1&gt;D.G0!,)%[9!*G/TY1HY!!!!1"`PYM6?4]4I2J^263)!8P1!!!!1!!!!!!!!!*Q!"4&amp;:$1Q!!!!%!!F:*4%)!!!!!5&amp;2)-!!!!!5!!1!"!!!!!!)!!Q!!!!!#!!%!!!!!!#!!!!!9?*RD9'.A;G#YQ!$%D%!7EQ;1^9&amp;"A!%!/C%%VA!!!%9!!!%9?*RD9-!%`Y%!3$%S-$"&gt;!.)M;/*A'M;G*M"F,C[\I/,-5$?SQI3"\NY$J*F!=F!V5$]QH1(C%_DG='!R'Q#!"SAG!!!!!!!-!!&amp;73524!!!!!!!$!!!"B!!!!R2YH'NA:'$).,9QOQ#EG9&amp;9H+'")4E`*:7,!=BHA)!84!Q5AQ#I?6JIYI9($K="A2[`@!O9X`S'J^N&amp;2;#Z2E7#K63EWU&gt;&amp;J..(B;742?8&amp;H````T=@Y4H=\:&amp;TX.%'J,;&lt;!SB_X%7&amp;!]1"UCQA_H^A"EA6T,Q!I'E=$28+$#5MBA?C$B^P-''%7!QT-AK&lt;`4+&gt;1./!N!33"8!(I+HH!$J3Y/"$FOZ'$3#`&gt;S+)"!LR&gt;):Q3"RXY&gt;!2!`):4Y!-\/3"_9)$\LYQE!%F+A+&gt;*C#,7"BB&amp;H7T(8@1!0P,112#:5#I#AB6!+*WA&amp;VQB#0O-$Q]V\[_NQM5$GR)9?Q!R!V!$)JD:+T(Q-A!MJ!*3.:#V&gt;I!W5R1-6B=A&gt;A@I'Q.*$UCU+"E:,#(C[V'MJ-*,-@)=)9"JI[2Y2+5X1"V)UB-&amp;KBZ!J3N!G1H1.H;109"+.M)S";!MCU:Q1QQWQ\+PA"V#S\;W&gt;`&amp;&amp;3F)Q'E;FKQZA4AZN]$!1+][!"1/!*LUAPQ!!!#0!!!!X(C==W"A9-AUND"L!.,-D!Q-YAQ.$-HZ+;E-3'!,!W\1`%;AWU6&amp;J..&amp;B;@&lt;2U7CUU?&amp;IZOBGR.)MX3SK,TY]````^9$`&amp;-/]']\M1.I0!/`[](G)Q+(Y@L8PL[X#S4/C'3G!]AN$,RA-39AFE=3"Q&amp;H@R&gt;8&gt;0?"V()#=8*OA9'"8H6!,:!.!0?1)OI!!!!!$B="A"!!!!9R.SYQ,D%!!!!!!!!-&amp;Q#!!!!!"$%X,D!!!!!!$B="A"!!!!9R.SYQ,D%!!!!!!!!-&amp;Q#!!!!!"$%X,D!!!!!!$B="A"!!!!9R.SYQ,D%!!!!!!!!5!1!!!068.9*Z*K+-,H.34A:*/:U!!!!.!!!!!!!!!!!!!!!!!!!!!!!!!)$`````A!!!!9=S,O')3C3"BEIEQ9&amp;3*)'/+[4BA!!!!@````_!!!!"A!!!!9!!!!'!!!!"A!!!!9!!!!'!!!)"A!!/!9!!0A'!!(Y"A!$_!9!!`A'!!0Y"A!$_!9!!`A'!!0TBA!$R_9!!R_'!!"`"A!"`!9!!0!'!!!!"`````Q!!!A$`````````````````````]2%2%2%2%2%2%2%2%2%2(`%2(`]2`R(R%@(`]@`R%2`R%@%2(R(R]2(R(R(R%2%@]2%@]2]2]@%2]2]2`R%2(`%2%2]@(R(R%@%@%@%2%2`R%@`R%@(R``(R(R(`]2%@]2%2%2%2%2%2%2%2%2%2(``````````````````````Q!!!!!!!!!!!!!!!!!!!0]!!!!!!!!!%1!!!!!!!!$`!!!!!!!!'\OR!!!!!!!!`Q!!!!!!'\%2'\%!!!!!!0]!!!!!'\%2%2%&lt;M1!!!!$`!!!!#\%2%2%2%2OQ!!!!`Q!!!!OR%2%2%2%@M!!!!0]!!!!,O\%2%2%@`\!!!!$`!!!!#\O\M2%@``_Q!!!!`Q!!!!O\O\O\````M!!!!0]!!!!,O\O\P````\!!!!$`!!!!#\O\O\````_Q!!!!`Q!!!!O\O\O`````M!!!!0]!!!!,O\O\P````\!!!!$`!!!!#\O\O\````_Q!!!!`Q!!!!O\O\O````\P`]!!0]!!!!!O\O\P``\O````Q$`!!!!!!#\O\`\O````Q!!`Q!!!!!!!,O\M@````!!!0]!!!!!!!!!M@````!!!!$`!!!!!!!!!!!0``!!!!!!`Q!!!!!!!!!!!!!!!!!!!0`````````````````````Q!!"!$```````````````````````````````````````````]&amp;"15&amp;"15&amp;"15&amp;"15&amp;"15&amp;"15&amp;"15&amp;"15&amp;"15&amp;"18``Q5&amp;"18```]&amp;"@``"18`"15&amp;`Q8```]&amp;````"15&amp;"@``"15&amp;`Q5&amp;"18`"18`"@]&amp;"18`"18`"18`"15&amp;"15&amp;``]&amp;"15&amp;``]&amp;"@]&amp;"@]&amp;`Q5&amp;"@]&amp;"@]&amp;"@``"15&amp;"18``Q5&amp;"15&amp;"@]&amp;`Q8`"18`"15&amp;`Q5&amp;`Q5&amp;`Q5&amp;"15&amp;"@``"15&amp;````"15&amp;`Q8`"@```Q8`"18`"18```]&amp;"15&amp;``]&amp;"15&amp;"15&amp;"15&amp;"15&amp;"15&amp;"15&amp;"15&amp;"15&amp;"15&amp;"18`````````````````````````````````````````````!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!!!!!!!!!!!!!5&amp;!!!!!!!!!!!!!!!!!!$``Q!!!!!!!!!!!!!!!!6:,VF:"1!!!!!!!!!!!!!!!0``!!!!!!!!!!!!!!6:,Q5&amp;"16:715!!!!!!!!!!!!!``]!!!!!!!!!!!6:,Q5&amp;"15&amp;"15&amp;76E&amp;!!!!!!!!!!$``Q!!!!!!!!":,Q5&amp;"15&amp;"15&amp;"15&amp;"6F:!!!!!!!!!0``!!!!!!!!!#]P"15&amp;"15&amp;"15&amp;"15&amp;L6E!!!!!!!!!``]!!!!!!!!!,VF:,Q5&amp;"15&amp;"15&amp;L;WN,Q!!!!!!!!$``Q!!!!!!!!!P76F:73]&amp;"15&amp;L;WNL;UP!!!!!!!!!0``!!!!!!!!!#^:76F:76EP7;WNL;WNL3]!!!!!!!!!``]!!!!!!!!!,VF:76F:76GNL;WNL;WN,Q!!!!!!!!$``Q!!!!!!!!!P76F:76F:7;WNL;WNL;UP!!!!!!!!!0``!!!!!!!!!#^:76F:76F:L;WNL;WNL3]!!!!!!!!!``]!!!!!!!!!,VF:76F:76GNL;WNL;WN,Q!!!!!!!!$``Q!!!!!!!!!P76F:76F:7;WNL;WNL;UP!!!!!!!!!0``!!!!!!!!!&amp;F:76F:76F:L;WNL;WN76GML+Q!!!!!``]!!!!!!!!!!#]P76F:76GNL;WN76EPL+SML+SM!!$``Q!!!!!!!!!!!!!P76F:7;WN76EPL+SML+SM!!!!!0``!!!!!!!!!!!!!!!!,VF:76E&amp;L+SML+SML!!!!!!!``]!!!!!!!!!!!!!!!!!!#]&amp;L+SML+SML!!!!!!!!!$``Q!!!!!!!!!!!!!!!!!!!!!!L+SML!!!!!!!!!!!!0``!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!````````````````````````````````````````````!!!!$!!"2F")5!!!!!!!!Q!!!]=!!!A]?*SN65N-%VU50H=9]":+O)/C.'*GK..KKE2DIC#__/7#_!BCQ0AA5;N4(QE21^'9G+AR%R*)7#A*CT^RQY+&amp;GX`2R&lt;]VJH%T#VW:K)MK]&lt;(&amp;"6(2[8DO$.-8I&lt;KQC]GE_&lt;\TH@/&gt;\^Y"I'WM5=L#82M)7]#89T:5'R9"3,&gt;170J&amp;RY#&gt;*$_!L!E2'TLI3@:7SJ)G'Y+'&amp;;8&lt;_4D-)^JZZUT#&amp;(RH\R&amp;;R5*9L.K'/M.;JRT2-UT`LUE@L`3LVM&amp;[.EGSUGF&gt;`5IHT7%5"(/T?#IN*!O%.]OS'?[,8U_9OPAXU%*$&lt;MG!$9R&lt;N3.[:B.72/FH&lt;EES#$.ES#]*7()TJ&amp;+J0%HR3&amp;'XD4&lt;"G='S+4*9BF00L1V*0&lt;06Z63\(.2*_4J=T5[*XA7JF,K;7QSJS,PNDTTD;X(6\H"Z=X.TS-0H%O_K$7PUT%%;IO_..Z7V*]T`A1"*H[@/;_?*Y#O^9AMONBY8%7EHL"`@_WW)G::U!72@[R_9=.=A_WP9+^&lt;1\;YBQ-=:Z0=Q]:M^=$E&gt;XN!Z&gt;$-ZGBD2BC^LFY&lt;CS;2W9_4;L@BI1D0CI`(F'^L(L6#LG&amp;[)O?'!V3$$0BAL&gt;(M9:G&gt;HU1"]ZKH\E&lt;J/T_2Y&gt;5P4_)Y&lt;?=?&amp;;N[Z!_A=`T:Q2&lt;A8;:?+-\MNH^E&gt;G&amp;G&amp;F&lt;:6F.G&gt;@T_TOT"+&gt;UMS#Q]A"F.F]N@KE1IS'U07(HB1BN/'H(N&amp;G9WBTBZ@JVRG&gt;S`0&lt;-T8SG6W?HK[C)@/N_=S+R0C:&gt;:][@RU@ILE,DI@I"6G#Z)\C/MQL%IXA]Y8&amp;&amp;G0)M,`6&lt;\`H3O'^64Z"7B6Y!77==TFR8ASU8VN+.%8(\V;MB%5Y2CU(8KGJC#D$6!,5&lt;DA4_QI\M3,CYMY-4Y^%&lt;-:\O/?&gt;@&amp;0LF\1BCYU(ONN]8I/CHK.5!FBK0(]5F1=57)BEKX1UKIX5$65G:JYQ;))9?%FY3!.C])L12X(S5&amp;L"$3AZL@9T;U'0(P%[_/J.Z?-=Q6^\R:7#M#BEL/HO!EQ@H0WDNP19VB6)?!:N&lt;^A9QJ=ROG^V5K_3U&gt;1IF\0.0A3YK&gt;!5W%`LF&amp;S]@1#ILG1)K0MT]O.7AH[[=_B(`]=_K()@DQ-2`%Q2-Z!Z#TQ&lt;]%/\T+KQ-NI\*SIE=@V&amp;O!'ZC*X*!]K2]=KF(F5HJ"674/&lt;C92@YH#Q&lt;["H/X9BCV&lt;ED&lt;)?G%@6XI*LA,Z^@D0XP;&lt;^/H@+!6A8.H(0BA&lt;?7@_#(5;`A`1Q\7+0]9L%WX)N@5Q8UK^S8`4UM2T4@!E0S30^8Z5W`A)HQ&amp;#4!!!!!!1!!!!N!!!!"!!!!!!!!!!-!!&amp;#2%B1!!!!!!!$!!!!9A!!!(*YH'.A9-A4E'$[RV$XFY&amp;*Y#O1)@W8A6H1D`%X!Q/HH]"B)-UI)!E5FPX,Q#[I$2&lt;70K,,Q1!&amp;KGS-(*)=BQ5ZQ$)=,2I-````Z`B[Z"J=R2%@/&amp;.FFDS("!!59BE!!!!!!!!%!!!!"Q!!!HA!!!!(!!!!)6^O;6^-98.U3WZP&gt;WZ0&gt;WZJ&lt;G&gt;-6E.M98.T1WRV=X2F=A!!!&amp;98!)!!!!!!!1!)!$$`````!!%!!!!!!$I!!!!#!"J!-P````]12'&amp;U97*B=W6';7RF5'&amp;U;!!!'%"1!!%!!!^T=7RJ&gt;'5T,GRW9WRB=X-!!1!"!!!!!!!!!"J-6E.M98.T5(*J&gt;G&amp;U:52B&gt;'&amp;597*0=G2F=A!!!#E8!)!!!!!!!A!&amp;!!=!!!Q!1!!"`````Q!!!!%!!1!!!!%!!!!!!!!!!!!!!"N-6E.M98.T5(*J&gt;G&amp;U:52B&gt;'&amp;5;7VF=X2B&lt;8!!!!!:&amp;Q#!!!!!!!%!"1!(!!!"!!$8P#[2!!!!!!!!!#:-6E.M98.T5(*J&gt;G&amp;U:52B&gt;'&amp;-98.U18"Q&lt;'FF:&amp;2J&lt;76T&gt;'&amp;N=!!!!"E8!)!!!!!!!1!&amp;!!=!!!%!!.?],J%!!!!!!!!!'ER71WRB=X.1=GFW982F2'&amp;U962Z='6%:8.D!!!!6B=!A!!!!!!"!!A!-0````]!!1!!!!!!/A!!!!)!'E!S`````R"%982B9G&amp;T:5:J&lt;'61982I!!!91&amp;!!!1!!$X.R&lt;'FU:4-O&lt;(:D&lt;'&amp;T=Q!"!!%!!!!!!!!!(ER71WRB=X.1=GFW982F2'&amp;U952G&lt;(2%982B5WF[:1!!!"E8!)!!!!!!!1!&amp;!!-!!!%!!!!!!!1!!!!!!!!!'ER71WRB=X.1=GFW982F2'&amp;U952G&lt;(2%982B!!!!4B=!A!!!!!!#!"J!-P````]12'&amp;U97*B=W6';7RF5'&amp;U;!!!'%"1!!%!!!^T=7RJ&gt;'5T,GRW9WRB=X-!!1!"5&amp;2)-!!!!!1!!!!!!!!!!!!!!!1!"!!)!!!!"!!!!%Y!!!!I!!!!!A!!"!!!!!!*!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!0]!!!'.?*S.DTV/QU!1B4^\]_0]G*D@!AH*."15+=A&amp;,)&amp;37^15&lt;/)V7&amp;L*%']C3K\!@&lt;A(VS!HA,'4+!5.][32XJP2GT@!'?@*T9^5&gt;+?&gt;HOH+4!NL5OW?Y9(VV_@Y!V"B^7I,:S:DO\,&amp;&lt;,2H=[OL;L$D=W?Z4F)]O,CVS]K:26TG=&lt;-5PSS+F89GTO11&lt;1&amp;^-P#_]97I,76#CV\32?8WC3"Z6_7^WQCGT)H%8&gt;'B3[#775Z!EZZB=\7PX&amp;MG0KF9^BBQ^=]M0N)[&gt;:0NA%?*U7)K.D%(D'2=)R*N"X_,P]J_=NBA]Z4(E3CV[[8%]O3&amp;.E.#DDG2(H)+P](G3"-!!!!!8Q!"!!)!!Q!%!!!!3!!."!!!!!!.!.!!O1!!!%]!$11!!!!!$1$1!,E!!!"7!!U%!!!!!!U!U!#Z!!!!89!!B!#!!!!.!.!!O1:597BP&lt;7%'6'&amp;I&lt;WVB"F2B;'^N91%Q!&amp;*45E-.#A!$4&amp;:$1UR#6F=!!".Y!!!%3!!!!#!!!".9!!!!!!!!!!!!!!!A!!!!.!!!"$Q!!!!=4%F#4A!!!!!!!!&amp;A4&amp;:45A!!!!!!!!&amp;U5F242Q!!!!!!!!')1U.46!!!!!!!!!'=4%FW;1!!!!!!!!'Q1U^/5!!!!!!!!!(%6%UY-!!!!!!!!!(92%:%5Q!!!!!!!!(M4%FE=Q!!!!!!!!)!6EF$2!!!!!%!!!)5&gt;G6S=Q!!!!1!!!)]5U.45A!!!!!!!!+A2U.15A!!!!!!!!+U35.04A!!!!!!!!,);7.M.!!!!!!!!!,=;7.M/!!!!!!!!!,Q4%FG=!!!!!!!!!-%2F")9A!!!!!!!!-92F"421!!!!!!!!-M6F"%5!!!!!!!!!.!4%FC:!!!!!!!!!.51E2)9A!!!!!!!!.I1E2421!!!!!!!!.]6EF55Q!!!!!!!!/12&amp;2)5!!!!!!!!!/E466*2!!!!!!!!!/Y3%F46!!!!!!!!!0-6E.55!!!!!!!!!0A2F2"1A!!!!!!!!0U!!!!!0````]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!$`````!!!!!!!!!-Q!!!!!!!!!!0````]!!!!!!!!!Y!!!!!!!!!!!`````Q!!!!!!!!$I!!!!!!!!!!$`````!!!!!!!!!21!!!!!!!!!!0````]!!!!!!!!"(!!!!!!!!!!!`````Q!!!!!!!!&amp;!!!!!!!!!!!$`````!!!!!!!!!9Q!!!!!!!!!!0````]!!!!!!!!"H!!!!!!!!!!"`````Q!!!!!!!!-E!!!!!!!!!!4`````!!!!!!!!!\A!!!!!!!!!"`````]!!!!!!!!$T!!!!!!!!!!)`````Q!!!!!!!!0=!!!!!!!!!!H`````!!!!!!!!!`!!!!!!!!!!#P````]!!!!!!!!%!!!!!!!!!!!!`````Q!!!!!!!!15!!!!!!!!!!$`````!!!!!!!!"#Q!!!!!!!!!!0````]!!!!!!!!%1!!!!!!!!!!!`````Q!!!!!!!!4%!!!!!!!!!!$`````!!!!!!!!"MA!!!!!!!!!!0````]!!!!!!!!+T!!!!!!!!!!!`````Q!!!!!!!!L=!!!!!!!!!!$`````!!!!!!!!$KA!!!!!!!!!!0````]!!!!!!!!/M!!!!!!!!!!!`````Q!!!!!!!![Y!!!!!!!!!!$`````!!!!!!!!$MA!!!!!!!!!!0````]!!!!!!!!0-!!!!!!!!!!!`````Q!!!!!!!!]Y!!!!!!!!!!$`````!!!!!!!!%&lt;1!!!!!!!!!!0````]!!!!!!!!2P!!!!!!!!!!!`````Q!!!!!!!"(%!!!!!!!!!!$`````!!!!!!!!%@!!!!!!!!!!A0````]!!!!!!!!3^!!!!!!,=X&amp;M;82F-SZD&gt;'Q!!!!!</Property>
	<Property Name="NI.LVClass.Geneology" Type="Xml"><String>

<Name></Name>

<Val>!!!!!AVT=7RJ&gt;'5T,GRW&lt;'FC$X.R&lt;'FU:4-O&lt;(:D&lt;'&amp;T=V"53$!!!!!!!!!!!!!!!!!!#!!"!!!!!!!!!1!!!!%!&amp;E"1!!!/=X&amp;M;82F,GRW9WRB=X-!!!%!!!!!!!!!!!!!!1Z-97*73568)%^C;G6D&gt;!"16%AQ!!!!!!!!!!!!&amp;Q#!!!!!!!!!!!$``Q!!!!%!!!!!!!%"!!!!!1!71&amp;!!!!ZT=7RJ&gt;'5O&lt;(:D&lt;'&amp;T=Q!!!1!!!!!!!@````Y!!!!!!B"*2'&amp;U97*B=W5O&lt;(:M;7*Q%5F%982B9G&amp;T:3ZM&gt;G.M98.T!&amp;"53$!!!!!!!!!!!!!8!)!!!!!!!!!!!0``!!!!!1!!!!!!!A%!!!!#!"J!-P````]12'&amp;U97*B=W6';7RF5'&amp;U;!!!8A$RV\QOE1!!!!-2=X&amp;M;82F-V^"5%EO&lt;(:M;7)/=X&amp;M;82F,GRW9WRB=X-+=X&amp;M;82F,G.U&lt;!!K1&amp;!!!1!!(5.M&gt;8.U:8)A&lt;W9A9WRB=X-A=(*J&gt;G&amp;U:3"E982B!!%!!1!!!!(`````5&amp;2)-!!!!!1!!!!!!!!!!!!!!B"*2'&amp;U97*B=W5O&lt;(:M;7*Q%5F%982B9G&amp;T:3ZM&gt;G.M98.T!&amp;"53$!!!!!!!!!!!!!8!)!!!!!!!!!!!0``!!!!!1!!!!!!!!)!!!!#!"J!-P````]12'&amp;U97*B=W6';7RF5'&amp;U;!!!8A$RV\QOE1!!!!-2=X&amp;M;82F-V^"5%EO&lt;(:M;7)/=X&amp;M;82F,GRW9WRB=X-+=X&amp;M;82F,G.U&lt;!!K1&amp;!!!1!!(5.M&gt;8.U:8)A&lt;W9A9WRB=X-A=(*J&gt;G&amp;U:3"E982B!!%!!1!!!!(````_5&amp;2)-!!!!!1!!!!!!!!!!!!!!B"*2'&amp;U97*B=W5O&lt;(:M;7*Q%5F%982B9G&amp;T:3ZM&gt;G.M98.T!&amp;"53$!!!!!!!!!!!!!8!)!!!!!!!!!!!0``!!!!!1!!!!!!!!-!!!!#!"J!-P````]12'&amp;U97*B=W6';7RF5'&amp;U;!!!8A$RV\QOE1!!!!-2=X&amp;M;82F-V^"5%EO&lt;(:M;7)/=X&amp;M;82F,GRW9WRB=X-+=X&amp;M;82F,G.U&lt;!!K1&amp;!!!1!!(5.M&gt;8.U:8)A&lt;W9A9WRB=X-A=(*J&gt;G&amp;U:3"E982B!!%!!1!!!!(````_5&amp;2)-!!!!!1!!!!!!!!!!!!!!B"*2'&amp;U97*B=W5O&lt;(:M;7*Q%5F%982B9G&amp;T:3ZM&gt;G.M98.T!&amp;"53$!!!!!!!!!!!!!8!)!!!!!!!!!!!0``!!!!!1!!!!!!!!1!!!!#!"J!-P````]12'&amp;U97*B=W6';7RF5'&amp;U;!!!8A$RV\QOE1!!!!-2=X&amp;M;82F-V^"5%EO&lt;(:M;7)/=X&amp;M;82F,GRW9WRB=X-+=X&amp;M;82F,G.U&lt;!!K1&amp;!!!1!!(5.M&gt;8.U:8)A&lt;W9A9WRB=X-A=(*J&gt;G&amp;U:3"E982B!!%!!1!!!!(````_5&amp;2)-!!!!!1!!!!!!!!!!!!!!B"*2'&amp;U97*B=W5O&lt;(:M;7*Q%5F%982B9G&amp;T:3ZM&gt;G.M98.T!&amp;"53$!!!!!!!!!!!!!8!)!!!!!!!!!!!0``!!!!!1!!!!!!!!!!!!!#!"J!-P````]12'&amp;U97*B=W6';7RF5'&amp;U;!!!8A$RV\QOE1!!!!-2=X&amp;M;82F-V^"5%EO&lt;(:M;7)/=X&amp;M;82F,GRW9WRB=X-+=X&amp;M;82F,G.U&lt;!!K1&amp;!!!1!!(5.M&gt;8.U:8)A&lt;W9A9WRB=X-A=(*J&gt;G&amp;U:3"E982B!!%!!1!!!!(````_5&amp;2)-!!!!!1!!!!!!!!!!!!!!B"*2'&amp;U97*B=W5O&lt;(:M;7*Q%5F%982B9G&amp;T:3ZM&gt;G.M98.T!&amp;"53$!!!!!!!!!!!!!8!)!!!!!!!!!!!0``!!!!!1!!!!!!!1!!!!!#!"J!-P````]12'&amp;U97*B=W6';7RF5'&amp;U;!!!8A$RV\QOE1!!!!-2=X&amp;M;82F-V^"5%EO&lt;(:M;7)/=X&amp;M;82F,GRW9WRB=X-+=X&amp;M;82F,G.U&lt;!!K1&amp;!!!1!!(5.M&gt;8.U:8)A&lt;W9A9WRB=X-A=(*J&gt;G&amp;U:3"E982B!!%!!1!!!!(````_5&amp;2)-!!!!!1!!!!!!!!!!!!!!A^*2'&amp;U97*B=W5O&lt;(:M;7)2352B&gt;'&amp;C98.F,GRW9WRB=X.16%AQ!!!!!!!!!!!!&amp;Q#!!!!!!!!!!!!!!!1!!!!A=X&amp;M;82F-V^"5%EO&lt;(:M;7)[=X&amp;M;82F,GRW9WRB=X-!!!!B=X&amp;M;82F-V^"5%EO&lt;(:M;7)[=X&amp;M;82F-SZM&gt;G.M98.T!!!!(8.R&lt;'FU:4-O&lt;(:M;7)[=X&amp;M;82F-SZM&gt;G.M98.T!!!!)8.R&lt;'FU:4.@16"*,GRW&lt;'FC/H.R&lt;'FU:4-O&lt;(:D&lt;'&amp;T=Q</Val>

</String>

</Property>
	<Property Name="NI.LVClass.IsTransferClass" Type="Bool">false</Property>
	<Property Name="NI.LVClass.LowestCompatibleVersion" Type="Str">1.0.0.1</Property>
	<Property Name="NI.LVClass.ParentClassLinkInfo" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!"&lt;!!!!!A^*2'&amp;U97*B=W5O&lt;(:M;7)2352B&gt;'&amp;C98.F,GRW9WRB=X.16%AQ!!!!,1!"!!9!!!!*352B&gt;'&amp;C98.F#5F%982B9G&amp;T:2&amp;*2'&amp;U97*B=W5O&lt;(:D&lt;'&amp;T=Q!!!!!</Property>
	<Property Name="NI.SortType" Type="Int">3</Property>
	<Item Name="sqlite3.ctl" Type="Class Private Data" URL="sqlite3.ctl">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
	</Item>
	<Item Name="IDatabase" Type="Folder">
		<Item Name="Connection" Type="Folder">
			<Item Name="Open Connection.vi" Type="VI" URL="../IDatabase/Open Connection.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%^!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$2!=!!?!!!@$8.R&lt;'FU:4-O&lt;(:M;7)0=X&amp;M;82F-SZM&gt;G.M98.T!!JT=7RJ&gt;'5A&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!"J!-0````]12'&amp;U97*B=W6';7RF5'&amp;U;!!!-E"Q!"Y!!"].=X&amp;M;82F-SZM&gt;GRJ9A^T=7RJ&gt;'5T,GRW9WRB=X-!#8.R&lt;'FU:3"J&lt;A"5!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!=!#!)!!(A!!!U)!!!!!!!!!!!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!))!!!!EA!!!!!"!!E!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">20971648</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">9187328</Property>
			</Item>
			<Item Name="Close Connection.vi" Type="VI" URL="../IDatabase/Close Connection.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%D!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$2!=!!?!!!@$8.R&lt;'FU:4-O&lt;(:M;7)0=X&amp;M;82F-SZM&gt;G.M98.T!!JT=7RJ&gt;'5A&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!$*!=!!?!!!@$8.R&lt;'FU:4-O&lt;(:M;7)0=X&amp;M;82F-SZM&gt;G.M98.T!!FT=7RJ&gt;'5A;7Y!6!$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!%!!=#!!"Y!!!.#!!!!!!!!!!!!!#.#Q!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!!!!!!!!!!*)!!!!!!1!)!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">20971648</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">9187328</Property>
			</Item>
		</Item>
		<Item Name="Query" Type="Folder">
			<Item Name="Types" Type="Folder">
				<Item Name="Query with 1D String Result.vi" Type="VI" URL="../IDatabase/Query with 1D String Result.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;@!!!!$!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!""!-0````]'5X2S;7ZH!!!51%!!!@````]!"1:3:8.V&lt;(1!!$2!=!!?!!!@$8.R&lt;'FU:4-O&lt;(:M;7)0=X&amp;M;82F-SZM&gt;G.M98.T!!NT=7RJ&gt;'5T)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!":!-0````]-586F=HEA=X2S;7ZH!!!U1(!!(A!!(QVT=7RJ&gt;'5T,GRW&lt;'FC$X.R&lt;'FU:4-O&lt;(:D&lt;'&amp;T=Q!+=X&amp;M;82F-S"J&lt;A!!6!$Q!!Q!!Q!%!!9!"Q!%!!1!"!!%!!A!"!!*!!I$!!"Y!!!*!!!!!!!!!!E!!!#.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!##A!!!*!!!!!!!1!,!!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">33554592</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">8392720</Property>
				</Item>
				<Item Name="Query with 2D String Result.vi" Type="VI" URL="../IDatabase/Query with 2D String Result.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;D!!!!$!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!""!-0````]'5X2S;7ZH!!!91%!!!P``````````!!5'5G6T&gt;7RU!!!U1(!!(A!!(QVT=7RJ&gt;'5T,GRW&lt;'FC$X.R&lt;'FU:4-O&lt;(:D&lt;'&amp;T=Q!,=X&amp;M;82F-S"P&gt;81!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!71$$`````$&amp;&amp;V:8*Z)(.U=GFO:Q!!.%"Q!"Y!!"].=X&amp;M;82F-SZM&gt;GRJ9A^T=7RJ&gt;'5T,GRW9WRB=X-!#H.R&lt;'FU:4-A;7Y!!&amp;1!]!!-!!-!"!!'!!=!"!!%!!1!"!!)!!1!#1!+!Q!!?!!!#1!!!!!!!!!*!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!AI!!!#1!!!!!!%!#Q!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">33554592</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">8392720</Property>
				</Item>
				<Item Name="Query with no type.vi" Type="VI" URL="../IDatabase/Query with no type.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;2!!!!$!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!R!5Q&gt;798*J97ZU!$2!=!!?!!!@$8.R&lt;'FU:4-O&lt;(:M;7)0=X&amp;M;82F-SZM&gt;G.M98.T!!NT=7RJ&gt;'5T)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!!J!5Q25?8"F!!!71$$`````$&amp;&amp;V:8*Z)(.U=GFO:Q!!.%"Q!"Y!!"].=X&amp;M;82F-SZM&gt;GRJ9A^T=7RJ&gt;'5T,GRW9WRB=X-!#H.R&lt;'FU:4-A;7Y!!&amp;1!]!!-!!-!"!!&amp;!!9!"!!%!!1!"!!(!!A!#1!+!Q!!?!!!#1!!!!!!!!!*!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!#!!!!AI!!!#1!!!!!!%!#Q!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">8388624</Property>
				</Item>
				<Item Name="Query with Scalar String Result.vi" Type="VI" URL="../IDatabase/Query with Scalar String Result.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;,!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!""!-0````]'5G6T&gt;7RU!!!U1(!!(A!!(QVT=7RJ&gt;'5T,GRW&lt;'FC$X.R&lt;'FU:4-O&lt;(:D&lt;'&amp;T=Q!,=X&amp;M;82F-S"P&gt;81!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!71$$`````$&amp;&amp;V:8*Z)(.U=GFO:Q!!.%"Q!"Y!!"].=X&amp;M;82F-SZM&gt;GRJ9A^T=7RJ&gt;'5T,GRW9WRB=X-!#H.R&lt;'FU:4-A;7Y!!&amp;1!]!!-!!-!"!!&amp;!!9!"!!%!!1!"!!(!!1!#!!*!Q!!?!!!#1!!!!!!!!!*!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!AI!!!#1!!!!!!%!#A!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">33554592</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">8392720</Property>
				</Item>
				<Item Name="Query with 1D 64bit Integer Result.vi" Type="VI" URL="../IDatabase/Query with 1D 64bit Integer Result.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;=!!!!$!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!V!"!!'5G6T&gt;7RU!!!51%!!!@````]!"1:3:8.V&lt;(1!!$2!=!!?!!!@$8.R&lt;'FU:4-O&lt;(:M;7)0=X&amp;M;82F-SZM&gt;G.M98.T!!NT=7RJ&gt;'5T)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!":!-0````]-586F=HEA=X2S;7ZH!!!U1(!!(A!!(QVT=7RJ&gt;'5T,GRW&lt;'FC$X.R&lt;'FU:4-O&lt;(:D&lt;'&amp;T=Q!+=X&amp;M;82F-S"J&lt;A!!6!$Q!!Q!!Q!%!!9!"Q!%!!1!"!!%!!A!"!!*!!I$!!"Y!!!*!!!!!!!!!!E!!!#.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!##A!!!*!!!!!!!1!,!!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">33554592</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">8392720</Property>
				</Item>
				<Item Name="Query with 1D Double Result.vi" Type="VI" URL="../IDatabase/Query with 1D Double Result.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;=!!!!$!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!V!#A!(4H6N:8*J9Q!51%!!!@````]!"1:3:8.V&lt;(1!!$2!=!!?!!!@$8.R&lt;'FU:4-O&lt;(:M;7)0=X&amp;M;82F-SZM&gt;G.M98.T!!NT=7RJ&gt;'5T)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!":!-0````]-586F=HEA=X2S;7ZH!!!U1(!!(A!!(QVT=7RJ&gt;'5T,GRW&lt;'FC$X.R&lt;'FU:4-O&lt;(:D&lt;'&amp;T=Q!+=X&amp;M;82F-S"J&lt;A!!6!$Q!!Q!!Q!%!!9!"Q!%!!1!"!!%!!A!"!!*!!I$!!"Y!!!*!!!!!!!!!!E!!!#.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!##A!!!*!!!!!!!1!,!!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">33554592</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">8392720</Property>
				</Item>
				<Item Name="Query with 1D Integer Result.vi" Type="VI" URL="../IDatabase/Query with 1D Integer Result.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;=!!!!$!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!V!!Q!(4H6N:8*J9Q!51%!!!@````]!"1:3:8.V&lt;(1!!$2!=!!?!!!@$8.R&lt;'FU:4-O&lt;(:M;7)0=X&amp;M;82F-SZM&gt;G.M98.T!!NT=7RJ&gt;'5T)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!":!-0````]-586F=HEA=X2S;7ZH!!!U1(!!(A!!(QVT=7RJ&gt;'5T,GRW&lt;'FC$X.R&lt;'FU:4-O&lt;(:D&lt;'&amp;T=Q!+=X&amp;M;82F-S"J&lt;A!!6!$Q!!Q!!Q!%!!9!"Q!%!!1!"!!%!!A!"!!*!!I$!!"Y!!!*!!!!!!!!!!E!!!#.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!##A!!!*!!!!!!!1!,!!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">33554592</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">8392720</Property>
				</Item>
				<Item Name="Query with 2D 64bit Integer Result.vi" Type="VI" URL="../IDatabase/Query with 2D 64bit Integer Result.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;A!!!!$!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!V!"!!'5G6T&gt;7RU!!!91%!!!P``````````!!5'5G6T&gt;7RU!!!U1(!!(A!!(QVT=7RJ&gt;'5T,GRW&lt;'FC$X.R&lt;'FU:4-O&lt;(:D&lt;'&amp;T=Q!,=X&amp;M;82F-S"P&gt;81!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!71$$`````$&amp;&amp;V:8*Z)(.U=GFO:Q!!.%"Q!"Y!!"].=X&amp;M;82F-SZM&gt;GRJ9A^T=7RJ&gt;'5T,GRW9WRB=X-!#H.R&lt;'FU:4-A;7Y!!&amp;1!]!!-!!-!"!!'!!=!"!!%!!1!"!!)!!1!#1!+!Q!!?!!!#1!!!!!!!!!*!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!AI!!!#1!!!!!!%!#Q!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">33554592</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">8392720</Property>
				</Item>
				<Item Name="Query with 2D Double Result.vi" Type="VI" URL="../IDatabase/Query with 2D Double Result.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;A!!!!$!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!V!#A!(4H6N:8*J9Q!91%!!!P``````````!!5'5G6T&gt;7RU!!!U1(!!(A!!(QVT=7RJ&gt;'5T,GRW&lt;'FC$X.R&lt;'FU:4-O&lt;(:D&lt;'&amp;T=Q!,=X&amp;M;82F-S"P&gt;81!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!71$$`````$&amp;&amp;V:8*Z)(.U=GFO:Q!!.%"Q!"Y!!"].=X&amp;M;82F-SZM&gt;GRJ9A^T=7RJ&gt;'5T,GRW9WRB=X-!#H.R&lt;'FU:4-A;7Y!!&amp;1!]!!-!!-!"!!'!!=!"!!%!!1!"!!)!!1!#1!+!Q!!?!!!#1!!!!!!!!!*!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!AI!!!#1!!!!!!%!#Q!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">33554592</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">8392720</Property>
				</Item>
				<Item Name="Query with 2D Integer Result.vi" Type="VI" URL="../IDatabase/Query with 2D Integer Result.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;A!!!!$!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!V!!Q!(4H6N:8*J9Q!91%!!!P``````````!!5'5G6T&gt;7RU!!!U1(!!(A!!(QVT=7RJ&gt;'5T,GRW&lt;'FC$X.R&lt;'FU:4-O&lt;(:D&lt;'&amp;T=Q!,=X&amp;M;82F-S"P&gt;81!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!71$$`````$&amp;&amp;V:8*Z)(.U=GFO:Q!!.%"Q!"Y!!"].=X&amp;M;82F-SZM&gt;GRJ9A^T=7RJ&gt;'5T,GRW9WRB=X-!#H.R&lt;'FU:4-A;7Y!!&amp;1!]!!-!!-!"!!'!!=!"!!%!!1!"!!)!!1!#1!+!Q!!?!!!#1!!!!!!!!!*!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!AI!!!#1!!!!!!%!#Q!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">33554592</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">8392720</Property>
				</Item>
				<Item Name="Query with Scalar 64bit Integer Result.vi" Type="VI" URL="../IDatabase/Query with Scalar 64bit Integer Result.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;)!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!V!"!!'5G6T&gt;7RU!!!U1(!!(A!!(QVT=7RJ&gt;'5T,GRW&lt;'FC$X.R&lt;'FU:4-O&lt;(:D&lt;'&amp;T=Q!,=X&amp;M;82F-S"P&gt;81!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!71$$`````$&amp;&amp;V:8*Z)(.U=GFO:Q!!.%"Q!"Y!!"].=X&amp;M;82F-SZM&gt;GRJ9A^T=7RJ&gt;'5T,GRW9WRB=X-!#H.R&lt;'FU:4-A;7Y!!&amp;1!]!!-!!-!"!!&amp;!!9!"!!%!!1!"!!(!!1!#!!*!Q!!?!!!#1!!!!!!!!!*!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!AI!!!#1!!!!!!%!#A!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">33554592</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">8392720</Property>
				</Item>
				<Item Name="Query with Scalar Double Result.vi" Type="VI" URL="../IDatabase/Query with Scalar Double Result.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;)!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!V!#A!'5G6T&gt;7RU!!!U1(!!(A!!(QVT=7RJ&gt;'5T,GRW&lt;'FC$X.R&lt;'FU:4-O&lt;(:D&lt;'&amp;T=Q!,=X&amp;M;82F-S"P&gt;81!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!71$$`````$&amp;&amp;V:8*Z)(.U=GFO:Q!!.%"Q!"Y!!"].=X&amp;M;82F-SZM&gt;GRJ9A^T=7RJ&gt;'5T,GRW9WRB=X-!#H.R&lt;'FU:4-A;7Y!!&amp;1!]!!-!!-!"!!&amp;!!9!"!!%!!1!"!!(!!1!#!!*!Q!!?!!!#1!!!!!!!!!*!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!AI!!!#1!!!!!!%!#A!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">33554592</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">8392720</Property>
				</Item>
				<Item Name="Query with Scalar Integer Result.vi" Type="VI" URL="../IDatabase/Query with Scalar Integer Result.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;)!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!V!!Q!'5G6T&gt;7RU!!!U1(!!(A!!(QVT=7RJ&gt;'5T,GRW&lt;'FC$X.R&lt;'FU:4-O&lt;(:D&lt;'&amp;T=Q!,=X&amp;M;82F-S"P&gt;81!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!71$$`````$&amp;&amp;V:8*Z)(.U=GFO:Q!!.%"Q!"Y!!"].=X&amp;M;82F-SZM&gt;GRJ9A^T=7RJ&gt;'5T,GRW9WRB=X-!#H.R&lt;'FU:4-A;7Y!!&amp;1!]!!-!!-!"!!&amp;!!9!"!!%!!1!"!!(!!1!#!!*!Q!!?!!!#1!!!!!!!!!*!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!AI!!!#1!!!!!!%!#A!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">33554592</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">8392720</Property>
				</Item>
			</Item>
		</Item>
		<Item Name="List" Type="Folder">
			<Item Name="List Columns.vi" Type="VI" URL="../IDatabase/List Columns.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'8!!!!$A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!""!-0````]'5X2S;7ZH!!!91%!!!@````]!"1ND&lt;WRV&lt;7ZT)'^V&gt;!!U1(!!(A!!(QVT=7RJ&gt;'5T,GRW&lt;'FC$X.R&lt;'FU:4-O&lt;(:D&lt;'&amp;T=Q!+=X&amp;M;82F)'^V&gt;!!!'%!Q`````Q^G&gt;7ZD&gt;'FP&lt;C"S:82V=GY!)E"!!!,``````````Q!)%6"315&gt;.13"U97*M:3"J&lt;G:P!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!%E!Q`````QB597*M:3"J&lt;A!!-E"Q!"Y!!"].=X&amp;M;82F-SZM&gt;GRJ9A^T=7RJ&gt;'5T,GRW9WRB=X-!#8.R&lt;'FU:3"J&lt;A"5!0!!$!!$!!1!"A!(!!E!"!!%!!1!#A!%!!M!$!-!!(A!!!U)!!!!!!!!#1!!!)U,!!!*!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!)!!!!E!!!!!!"!!U!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">20971648</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">269234192</Property>
			</Item>
			<Item Name="List Tables.vi" Type="VI" URL="../IDatabase/List Tables.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;@!!!!$!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!""!-0````]'5X2S;7ZH!!!91%!!!@````]!"1J597*M:8-A&lt;X6U!!!U1(!!(A!!(QVT=7RJ&gt;'5T,GRW&lt;'FC$X.R&lt;'FU:4-O&lt;(:D&lt;'&amp;T=Q!+=X&amp;M;82F)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!51$$`````#U2B&gt;'&amp;C98.F)'FO!$*!=!!?!!!@$8.R&lt;'FU:4-O&lt;(:M;7)0=X&amp;M;82F-SZM&gt;G.M98.T!!FT=7RJ&gt;'5A;7Y!6!$Q!!Q!!Q!%!!9!"Q!%!!1!"!!%!!A!"!!*!!I$!!"Y!!!.#!!!!!!!!!E!!!#.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!##!!!!*!!!!!!!1!,!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">1094713472</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">9187344</Property>
			</Item>
			<Item Name="List Databases.vi" Type="VI" URL="../IDatabase/List Databases.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;B!!!!$!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!"B!-0````]/586F=HEA=X2S;7ZH)$)!!":!1!!"`````Q!&amp;#72B&gt;'&amp;C98.F=Q!U1(!!(A!!(QVT=7RJ&gt;'5T,GRW&lt;'FC$X.R&lt;'FU:4-O&lt;(:D&lt;'&amp;T=Q!+=X&amp;M;82F)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!11$$`````"U.P&lt;H2F?(1!-E"Q!"Y!!"].=X&amp;M;82F-SZM&gt;GRJ9A^T=7RJ&gt;'5T,GRW9WRB=X-!#8.R&lt;'FU:3"J&lt;A"5!0!!$!!$!!1!"A!(!!1!"!!%!!1!#!!%!!E!#A)!!(A!!!U)!!!!!!!!#1!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!))!!!!E!!!!!!"!!M!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">1094713472</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">9187328</Property>
			</Item>
		</Item>
		<Item Name="Metadata" Type="Folder">
			<Item Name="Metadata - table.vi" Type="VI" URL="../IDatabase/Metadata - table.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;P!!!!$!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!"B!-0````]0:H6O9X2J&lt;WYA=G6U&gt;8*O!#*!1!!#``````````]!"2&amp;15E&amp;(45%A&gt;'&amp;C&lt;'5A;7ZG&lt;Q!U1(!!(A!!(QVT=7RJ&gt;'5T,GRW&lt;'FC$X.R&lt;'FU:4-O&lt;(:D&lt;'&amp;T=Q!+=X&amp;M;82F)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!31$$`````#&amp;2B9GRF)'FO!!!S1(!!(A!!(QVT=7RJ&gt;'5T,GRW&lt;'FC$X.R&lt;'FU:4-O&lt;(:D&lt;'&amp;T=Q!*=X&amp;M;82F)'FO!&amp;1!]!!-!!-!"!!'!!=!"!!%!!1!"!!)!!1!#1!+!Q!!?!!!$1A!!!!!!!!*!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!A!!!#1!!!!!!%!#Q!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">20971648</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">9187344</Property>
			</Item>
		</Item>
	</Item>
	<Item Name="Construct sqlite3.vi" Type="VI" URL="../Construct sqlite3.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%,!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$2!=!!?!!!@$8.R&lt;'FU:4-O&lt;(:M;7)0=X&amp;M;82F-SZM&gt;G.M98.T!!NT=7RJ&gt;'5T)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!"J!-P````]12'&amp;U97*B=W6';7RF5'&amp;U;!!!6!$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!(!!1$!!"Y!!!.#!!!!!!!!!!!!!!*!!!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!#!!!!!!!!!!!!1!)!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1350578192</Property>
	</Item>
</LVClass>
